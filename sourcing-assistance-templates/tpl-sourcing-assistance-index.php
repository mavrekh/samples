<?php
//Template Name: Sourcing Assistance: Index
get_header();

if (have_posts()) {
	while (have_posts()) {
		the_post();
		update_post_caches($posts);

		require_once(get_template_directory() . '/inc/views/sourcing_assistance_index_view.php');
	}
}

get_footer(); 
?>
