<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');

if (!is_object($form)) {
	$form = new Zebra_Form('form');
}

// contact info
$form->add('label','label_saf_contact_info_name','saf_contact_info_name','Name');
$obj = $form->add('text','saf_contact_info_name', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  =>  array('error', 'Please enter your name'),
    ));


$form->add('label','label_saf_contact_info_email','saf_contact_info_email','Email');
$obj = $form->add('text','saf_contact_info_email', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  => array('error', 'Email is required!'),
		'email'     => array('error', 'Email address seems to be invalid!'),
    ));


$form->add('label','label_saf_contact_info_title','saf_contact_info_title','Title');
$obj = $form->add('text','saf_contact_info_title', '', array('class'=>'longbox'));


$form->add('label','label_saf_contact_info_business_name','saf_contact_info_business_name','Business Name');
$obj = $form->add('text','saf_contact_info_business_name', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  => array('error', 'Please enter your business name'),
   ));


$form->add('label','label_saf_contact_info_address','saf_contact_info_address','Street Address');
$obj = $form->add('text','saf_contact_info_address', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  => array('error', 'Please enter your street address'),
    ));


$form->add('label','label_saf_contact_info_city','saf_contact_info_city','City');
$obj = $form->add('text','saf_contact_info_city');
$obj->set_rule(array(
		'required'  => array('error', 'Please enter your city'),
    ));


$form->add('label','label_saf_contact_info_state','saf_contact_info_state','State');
$obj = $form->add('select','saf_contact_info_state');
$obj->add_options(array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming")
			);

$obj->set_rule(array(
        'required'  => array('error', 'Please enter your state'),
    ));


$form->add('label','label_saf_contact_info_zip','saf_contact_info_zip','Zip Code');
$obj = $form->add('text','saf_contact_info_zip');
$obj->set_rule(array(
	'required'  => array('error', 'Please enter your zip code'),
    ));
?>
