<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');
require_once(get_template_directory() . '/inc/models/sourcing_assistance_form_save.php');


$form = new Zebra_Form('saf_form');

// get the industry chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_select_industry.php');

// Materials form
$form->add("label","label_saf_source_exchange","saf_source_exchange","Are you looking to exchange a material or source material for production?");
$obj = $form->add('radios','saf_source_exchange',
		array('exchange' => 'Material Exchange',
			'source'	=>	'Source material for production'
			)
	);
$obj->set_rule(array(
	'required'  =>  array('error', 'Please answer this question')
));

// the exchange option
$form->add('label','label_saf_exch_product_desc','saf_exch_product_desc','Please describe your product');
$obj = $form->add('text','saf_exch_product_desc', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  =>  array('error', 'Please describe your product'),
        'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'exchange'
        ), 'toggleOpt, _exchange'),
    ));
	
$form->add('label','label_saf_material_used','saf_material_used','What material are you using now?');
$obj = $form->add('text','saf_material_used', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please answer this question'),
        'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'exchange',
        ), 'toggleOpt, _exchange'),
    ));
	
$form->add('label','label_saf_why_exchange','saf_why_exchange','Why are you looking to exchange?');
$obj = $form->add('text','saf_why_exchange', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please answer this question'),
        'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'exchange',
        ), 'toggleOpt, _exchange'),
    ));
	
$form->add('label','label_saf_mat_parameters','saf_mat_parameters','Please list any parameters for the new material you are seeking');
$obj = $form->add('text','saf_mat_parameters', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please answer this question'),
        'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'exchange',
        ), 'toggleOpt, _exchange'),
    ));
	
 

// the source option                   	
$form->add('label','label_saf_source_product_desc','saf_source_product_desc','Please describe your product');
$obj = $form->add('text','saf_source_product_desc', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  =>  array('error', 'Please describe your product'),
        'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'source',
        ), 'toggleOpt, _source'),
    ));


$form->add('label','label_saf_material_sourced','saf_material_sourced','What material are you looking to source?');
$obj = $form->add('text','saf_material_sourced', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please answer this question'),
        'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'source',
        ), 'toggleOpt, _source'),
    ));

$form->add("label","label_saf_source_sample","saf_source_sample","Do you have a sample?");
$obj = $form->add('radios','saf_source_sample',
		array('yes' => 'Yes',
			'no'	=>	'No'
			)
	);
$obj->set_rule(array(
        'required'  =>  array('error', 'Please answer this question'),
		'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'source',
        ), 'toggleOpt, _source'),
));

$form->add("label","label_saf_source_in_prod","saf_source_in_prod","Are you currently in production?");
$obj = $form->add('radios','saf_source_in_prod',
		array('yes' => 'Yes',
			'no'	=>	'No'
			)
	);
$obj->set_rule(array(
		'dependencies'   =>  array(array(
            'saf_source_exchange' =>  'source',
        ), 'toggleOpt, _source'),
        'required'  =>  array('error', 'Please answer this question')
));

// get the business description chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_your_business.php');

// get the contact info
include(get_template_directory() . '/inc/models/sourcing_assistance_contact_info.php');

$form->add('submit', 'saf_btnsubmit', 'Submit');


$form->validate();

if ($_POST) {
	if ($savedData = save_saf_form('saf_responses_materials')) {
		email_submitted_data($savedData, 'Materials Form');	

		include(get_template_directory() . '/inc/views/sourcing_assistance_success_view.php');
	} else {
	// todo: a page telling them what to do if there's a db glitch
	}
} else {
	$form->render( get_template_directory() . '/inc/views/sourcing_assistance_materials_view.php');
}

?>
