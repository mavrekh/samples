<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');
require_once(get_template_directory() . '/inc/models/sourcing_assistance_form_save.php');


$form = new Zebra_Form('form');

// get the industry chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_select_industry.php');

// prototype/design 
$form->add('label','label_saf_proto_product_idea','saf_proto_product_idea','Please describe your product idea');
$obj = $form->add('textarea','saf_proto_product_idea');
$obj->set_rule(array(
		'required'  =>  array('error', 'Please describe your product idea')
));

$form->add("label","label_saf_proto_needs","saf_proto_needs","I am looking for:");
$obj = $form->add('radios','saf_proto_needs',
		array('CAD' => 'CAD or other digital modeling', 
			'Rapid_Prototyping' => 'Rapid Prototyping',  
			'Sample_Making' => 'Sample Making', 
			'Pattern_Making' => 'Pattern Making', 
			'Casting' => 'Casting', 
			'Metal_Fabrication' => 'Metal Fabrication', 
			'Other_Product_Design' => 'Other Product Design'
			)
	);
$obj->set_rule(array(
		'required'  =>  array('error', 'Please select the area you might need help with')
));

// get the business description chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_your_business.php');

// get the contact info bit
include(get_template_directory() . '/inc/models/sourcing_assistance_contact_info.php');

$form->add('submit', 'saf_btnsubmit', 'Submit');

$form->validate();

if ($_POST) {
	if ($savedData = save_saf_form('saf_responses_prototyping')) {
		email_submitted_data($savedData, 'Product Design/Prototyping');	

		include(get_template_directory() . '/inc/views/sourcing_assistance_success_view.php');
	} else {
	// todo: a page telling them what to do if there's a db glitch
	}
} else {
	$form->render( get_template_directory() . '/inc/views/sourcing_assistance_prototype_view.php');
}


?>
