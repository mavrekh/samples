<?php

function save_saf_form($table) {
	global $wpdb;
	
	$available_tables = array('saf_responses_manufacturing','saf_responses_materials',
		'saf_responses_prototyping','saf_responses_space_needs','saf_responses_other_needs');

	if (!in_array($table,$available_tables)) {
		trigger_error("Cannot write to $table");
		return false;
	}

	$goodData = array();
	$cInfo = array();
	
	$avoidElements = array('name_saf_form','saf_btnsubmit','saf_manuf_sample_required','saf_nextbutton');

	// Filter POST params for known form keys
	foreach($_POST as $key => $value)
	{
		if (in_array($key, $avoidElements) || (strpos($key, 'zebra_') !== false) ) {
			continue;	// avoid buttons and hidden zebra forms elements (e.g. the honeypot)
		}

		// keep the contact info separate
		if (strpos($key, 'contact_info_') !== false) {
			$cInfo[str_replace('saf_','',str_replace('contact_info_','',$key))] = $value;
			continue;
		}

		// keep the business descriptio info separate
		if (strpos($key, 'yb_') !== false) {
			$busInfo[str_replace('saf_','',str_replace('yb_','',$key))] = $value;
			continue;
		}

		if(strpos($key, 'saf_') !== false) {
			if (is_array($value)) {
			// for checkboxes and such
				$value = implode(', ',$value);
			}
			
			$goodData[str_replace('saf_','',$key)] = $value;
		}
	}

	// save save save
	if (!empty($goodData)) {
		@$wpdb->insert($table, $goodData);
		$table_id = $wpdb->insert_id;
		$tableError = $wpdb->last_error;
		 
		if (!empty($cInfo)) {
			$cInfo['response_table_id'] = $busInfo['response_table_id'] = $table_id;
			$cInfo['response_table_name'] = $busInfo['response_table_name'] = $table;
			
			@$wpdb->insert('saf_responses_contact_info',$cInfo);
			@$wpdb->insert('saf_responses_business_description',$busInfo);

			$ciError = $wpdb->last_error;
			// todo: check if saved, say something nice
		} else {
			trigger_error('Contact info data is missing');
			return false;
		}

		if (!$tableError && !$ciError) {
			// if all is good, return merged saved data for emails etc.
			return array_merge($goodData, $cInfo, $busInfo);
		} else {
			trigger_error("Form data did not get saved for $table");
			return false;
		}	
	} else {
		return false;
	}
	
}


function email_submitted_data($data, $form = "") {
	$avoidElements = array('response_table_id','response_table_name');

	if (!$data) {
		trigger_error('No data to email in email_submitted_data');
		return;
	}

	$email = get_option('minyc_saf_email');
	$subject = get_option('minyc_saf_email_subject');

	if (!$email) {
		trigger_error('No email address set for email_submitted_data');
		return;
	}

	foreach ($avoidElements as $ae) {
		unset($data[$ae]);
	}
	
	$retStr = print_r($data, true);

	$retStr = str_replace('Array','Form Entries',$retStr);
	
	if ($form) {
		$retStr = "$form : $retStr";
	}

	mail($email, $subject, $retStr);
}

?>
