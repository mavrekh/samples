<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');
require_once(get_template_directory() . '/inc/models/sourcing_assistance_form_save.php');


$form = new Zebra_Form('form');

// get the industry chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_select_industry.php');

$form->add("label","label_saf_other_needs","saf_other_needs","Please describe your sourcing need");
$obj = $form->add('textarea','saf_other_needs');
$obj->set_rule(array(
		'required'  =>  array('error', 'Please describe your sourcing need')
		));

// get the business description chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_your_business.php');

// get the contact info
include(get_template_directory() . '/inc/models/sourcing_assistance_contact_info.php');

$form->add('submit', 'saf_btnsubmit', 'Submit');

$form->validate();

if ($_POST) {
	if ($savedData = save_saf_form('saf_responses_other_needs')) {
		email_submitted_data($savedData, 'Other Sourcing Needs');	

		include(get_template_directory() . '/inc/views/sourcing_assistance_success_view.php');
	} else {
	// todo: a page telling them what to do if there's a db glitch
	}
} else {
	$form->render( get_template_directory() . '/inc/views/sourcing_assistance_other_view.php');
}


?>
