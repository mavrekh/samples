<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');
require_once(get_template_directory() . '/inc/models/sourcing_assistance_form_save.php');

$form = new Zebra_Form('form');

// get the industry chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_select_industry.php');


$form->add("label","label_saf_manuf_has_sample","saf_manuf_has_sample",'Do you have a sample, complete design, or working prototype?');
$obj = $form->add('radios','saf_manuf_has_sample',
		array('yes' => 'Yes',
			'no'	=>	'No'
			)
	);
$obj->set_rule(array(
	'required'  =>  array('error', 'Please select a response')
));

$form->add("label","label_saf_manuf_working_with_manufacturer","saf_manuf_working_with_manufacturer",'Are you currently working with a manufacturer?');
$obj = $form->add('radios','saf_manuf_working_with_manufacturer',
		array('yes' => 'Yes',
			'no'	=>	'No'
			)
	);
$obj->set_rule(array(
	'required'  =>  array('error', 'Please select a response')
));



$form->add('label','label_saf_manuf_name','saf_manuf_name','Name of Manufacturer');
$obj = $form->add('text','saf_manuf_name', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_working_with_manufacturer' =>  'yes'
        ), 'toggleOpt, _withmanuf_yes'),
    ));

$form->add('label','label_saf_manuf_address','saf_manuf_address','Address');
$obj = $form->add('text','saf_manuf_address', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_working_with_manufacturer' =>  'yes'
        ), 'toggleOpt, _withmanuf_yes'),
    ));

$form->add('label','label_saf_manuf_address_city','saf_manuf_address_city','City');
$obj = $form->add('text','saf_manuf_address_city');
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_working_with_manufacturer' =>  'yes'
        ), 'toggleOpt, _withmanuf_yes'),
    ));


$form->add('label','label_saf_manuf_address_state','saf_manuf_address_state','State');
$obj = $form->add('select','saf_manuf_address_state');
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_working_with_manufacturer' =>  'yes'
        ), 'toggleOpt, _withmanuf_yes'),
    ));
$obj->add_options(array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming")
			);

$form->add('label','label_saf_manuf_address_zip','saf_manuf_address_zip','Zip');
$obj = $form->add('text','saf_manuf_address_zip');
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_working_with_manufacturer' =>  'yes'
        ), 'toggleOpt, _withmanuf_yes'),
    ));






$form->add('label','label_saf_manuf_product_desc','saf_manuf_product_desc','Please describe your product');
$obj = $form->add('text','saf_manuf_product_desc', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please describe your product'),
        'dependencies'   =>  array(array(
            'saf_manuf_has_sample' =>  'yes'
        ), 'toggleOpt, _sample_yes'),
    ));
	


$form->add('label','label_saf_manuf_list_materials','saf_manuf_list_materials','Please list your materials used in production:');
$obj = $form->add('text','saf_manuf_list_materials', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_has_sample' =>  'yes'
        ), 'toggleOpt, _sample_yes'),
    ));
	


$form->add('label','label_saf_manuf_list_equipment','saf_manuf_list_equipment','Please list any specialized equipment needed for production');
$obj = $form->add('text','saf_manuf_list_equipment', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_has_sample' =>  'yes'
        ), 'toggleOpt, _sample_yes'),
    ));
	

$form->add('label','label_saf_manuf_quantities','saf_manuf_quantities','What quantities of product are you looking to manufacture?');
$obj = $form->add('text','saf_manuf_quantities');
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
        'dependencies'   =>  array(array(
            'saf_manuf_has_sample' =>  'yes'
        ), 'toggleOpt, _sample_yes'),
    ));


$form->add('label', 'label_saf_manuf_sample_required', 'saf_manuf_sample_required', 'Required!');
$obj = $form->add('hidden','saf_manuf_sample_required');
$obj->set_rule(array(
        'dependencies'   =>  array(array(
            'saf_manuf_has_sample' =>  'no'
        ), 'toggleOpt, _sample_no'),
    ));

// get the business description chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_your_business.php');

// get the contact info chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_contact_info.php');



$form->add('submit', 'saf_btnsubmit', 'Submit');

$form->validate();

if ($_POST) {
	if ($savedData = save_saf_form('saf_responses_manufacturing')) {
		email_submitted_data($savedData, 'Manufacturing');	

		include(get_template_directory() . '/inc/views/sourcing_assistance_success_view.php');
	} else {
	// todo: a page telling them what to do if there's a db glitch
	}
} else {
	$form->render( get_template_directory() . '/inc/views/sourcing_assistance_manufacturing_view.php');
}


?>
