<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');

if (!is_object($form)) {
	$form = new Zebra_Form('form');
}


$form->add("label","label_saf_yb_registered_minyc","saf_yb_registered_minyc",'Is your business already registered with Made in NYC?');
$obj = $form->add('radios','saf_yb_registered_minyc',
		array('yes' => 'Yes',
			'no'	=>	'No'
			)
	);
$obj->set_rule(array(
	'required'  =>  array('error', 'Please select a response')
));



$form->add('label','label_saf_yb_years_in_business','saf_yb_years_in_business','Number of years in business?');
$obj = $form->add('text','saf_yb_years_in_business');
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response')
));


$form->add('label','label_saf_yb_employees','saf_yb_employees','Number of employees');
$obj = $form->add('text','saf_yb_employees');
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response')
));



$form->add('label','label_saf_yb_how_did_you_hear','saf_yb_how_did_you_hear','How did you hear about our website/services?');
$obj = $form->add('text','saf_yb_how_did_you_hear', '', array('class'=>'longbox'));
$obj->set_rule(array(
	'required'  =>  array('error', 'Please include a response'),
    ));


?>