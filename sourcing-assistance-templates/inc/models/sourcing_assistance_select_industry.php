<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');

if (!is_object($form)) {
	$form = new Zebra_Form('form');
}

$form->add("label","label_saf_industry","saf_industry",'Select an Industry');
$obj = $form->add('radios','saf_industry',
		array(
			'Construction' => 'Construction',
			'Interiors' => 'Interiors',
			'Apparel_and_Jewelry' => 'Apparel and Jewelry',
			'Food' => 'Food',
			'Machinery_and_Metals' => 'Machinery and Metals',
			'Printing' => 'Printing',
			'Other' => 'Other',
			)
	);

$form->add('button', 'saf_nextbutton', 'Next');

?>
