<?php
require_once(get_template_directory() . '/inc/libraries/Zebra_Form-master/Zebra_Form.php');
require_once(get_template_directory() . '/inc/models/sourcing_assistance_form_save.php');

$form = new Zebra_Form('form');

// get the industry chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_select_industry.php');


$form->add("label","label_saf_sq_feet",'saf_sq_feet','How many sq ft are you looking for?');
$obj = $form->add('text','saf_sq_feet');
$obj->set_rule(array(
		'required'  =>  array('error', 'Please enter an approximate number of sq feet you need')
		));



$form->add("label","label_saf_space_attributes","saf_space_attributes","Please check any of the following attributes that are necessary or desired for your business:", array('class'=>'checkbox-label'));
$obj = $form->add('checkboxes','saf_space_attributes[]',
		array('Ground_floor'	=>	'Ground floor',
			'Freight_elevator'	=>	'Freight elevator',
			'High_ceilings'		=>	'High ceilings',
			'Loading_dock'		=>	'Loading dock',
			'Off_street_parking'=>	'Off-street parking',
			'Weight_bearing_floors'	=>	'Weight-bearing floors',
			'FiOS_Broadband_Connected'	=>	'FiOS or Broadband Connected'
			),
		array("class" =>"css-checkbox")
		);
$obj->set_rule(array(
		'required'  =>  array('error', 'Please select one or more items describing your space needs')
		));


$form->add("label","label_saf_space_needs","saf_space_needs","Why are you seeking new space?", array('class'=>'checkbox-label'));

$obj = $form->add('checkboxes','saf_space_needs[]',
		array('Looking_for_First_Space' =>'I am looking for my first location',
			'Rent_is_too_high' => 'The rent at my current location is increasing too fast',
			'Current_Space_Too_Large'	=> 	'My current location is too large and I need a smaller space',
			'Cannot_expand' => 'I need to expand my business and cannot do so at current location due to space limitations',
			'Landlord_Request_To_Leave' =>	'My landlord asked me to leave',
			'Building_Rezoned' => 'The building was re-zoned',
			'Area_Rezoned' => 'The area was re-zoned'
			),
		array("class" =>"css-checkbox")
	);

$obj->set_rule(array(
		'required'  =>  array('error', 'Please select one or more items describing your space needs')
		));



$form->add("label","label_saf_special_reqs_certs",'saf_special_reqs_certs','Are you seeking space with any special requirements or certifications?  Please elaborate:');
$obj = $form->add('text','saf_special_reqs_certs', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  =>  array('error', 'Please elaborate or enter N/A')
		));

$form->add("label","label_saf_specific_location", 'saf_specific_location', 'Is there a neighborhood or specific location within the city you are particularly interested in?');
$obj = $form->add('text','saf_specific_location', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  =>  array('error', 'Please elaborate or enter N/A')
		));


$form->add("label","label_saf_current_address","saf_current_address","Address");
$obj = $form->add('text','saf_current_address', '', array('class'=>'longbox'));
$obj->set_rule(array(
		'required'  =>  array('error', 'Please enter your current address')
		));


$form->add("label","label_saf_current_city","saf_current_city","City");
$obj = $form->add('text','saf_current_city');
$obj->set_rule(array(
		'required'  =>  array('error', 'Please enter your current address')
		));


$form->add("label","label_saf_current_state","saf_current_state","State");
$obj = $form->add('select','saf_current_state');
$obj->add_options(array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming")
			);

$obj->set_rule(array(
		'required'  =>  array('error', 'Please enter your current address')
		));


$form->add("label","label_saf_current_zip","saf_current_zip","Zip");
$obj = $form->add('text','saf_current_zip');
$obj->set_rule(array(
		'required'  =>  array('error', 'Please enter your current address')
		));



// get the business description chunk
require_once(get_template_directory() . '/inc/models/sourcing_assistance_your_business.php');

// get the contact info bit
include(get_template_directory() . '/inc/models/sourcing_assistance_contact_info.php');

$form->add('submit', 'saf_btnsubmit', 'Submit');

$form->validate();

if ($_POST) {
	if ($savedData = save_saf_form('saf_responses_space_needs')) {
		email_submitted_data($savedData, 'Space Needs');	

		include(get_template_directory() . '/inc/views/sourcing_assistance_success_view.php');
	} else {
	// todo: a page telling them what to do if there's a db glitch
	}
} else {
	$form->render( get_template_directory() . '/inc/views/sourcing_assistance_space_view.php');
}

?>
