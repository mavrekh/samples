<div class="saf-outer">
	<div id="container" class="no_sidebar">

	<h2>Space</h2>

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_select_industry_view.php'); ?>	

	<div id="space_needs" class="showable">
	
	<?php print $label_saf_sq_feet; ?>
	<?php print $saf_sq_feet; ?>

	<span class="checkbox-label"><?php print $label_saf_space_attributes; ?></span>

	<span class="checkbox-unit">
		<?php print $saf_space_attributes_Ground_floor; ?>
		<?php print $label_saf_space_attributes_Ground_floor; ?>
	</span>
	
	<span class="checkbox-unit">
		<?php print $saf_space_attributes_Freight_elevator; ?>
		<?php print $label_saf_space_attributes_Freight_elevator; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_space_attributes_High_ceilings; ?>
		<?php print $label_saf_space_attributes_High_ceilings; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_space_attributes_Loading_dock; ?>
		<?php print $label_saf_space_attributes_Loading_dock; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_space_attributes_Off_street_parking; ?>
		<?php print $label_saf_space_attributes_Off_street_parking; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_space_attributes_Weight_bearing_floors; ?>
		<?php print $label_saf_space_attributes_Weight_bearing_floors; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_space_attributes_FiOS_Broadband_Connected; ?>
		<?php print $label_saf_space_attributes_FiOS_Broadband_Connected; ?>
	</span>

	<div class="space-above space-below"></br></div>
	
	<?php print $label_saf_special_reqs_certs; ?>
	<?php print $saf_special_reqs_certs; ?>

	<?php print $label_saf_specific_location; ?>
	<?php print $saf_specific_location; ?>

	<span class="checkbox-label"><?php print $label_saf_space_needs; ?></span>

	<span class="checkbox-unit">
		<?php print $saf_space_needs_Looking_for_First_Space; ?>
		<?php print $label_saf_space_needs_Looking_for_First_Space; ?>
	</span>
	
	<span class="checkbox-unit">
		<?php print $saf_space_needs_Rent_is_too_high; ?>
		<?php print $label_saf_space_needs_Rent_is_too_high; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_space_needs_Current_Space_Too_Large; ?>
		<?php print $label_saf_space_needs_Current_Space_Too_Large; ?>
	</span>
		
	<span class="checkbox-unit">
		<?php print $saf_space_needs_Cannot_expand; ?>
		<?php print $label_saf_space_needs_Cannot_expand; ?>
	</span>
	
	<span class="checkbox-unit">
		<?php print $saf_space_needs_Landlord_Request_To_Leave; ?>
		<?php print $label_saf_space_needs_Landlord_Request_To_Leave; ?>
	</span>
	
	<span class="checkbox-unit">
		<?php print $saf_space_needs_Building_Rezoned; ?>
		<?php print $label_saf_space_needs_Building_Rezoned; ?>
	</span>
	
	<span class="checkbox-unit">
		<?php print $saf_space_needs_Area_Rezoned; ?>
		<?php print $label_saf_space_needs_Area_Rezoned; ?>
	</span>


	
	<span class="space-above space-below">Where are you currently located?</span>
	
	<?php print $label_saf_current_address; ?>
	<?php print $saf_current_address; ?>
	
	<div id="city">
		<?php print $label_saf_current_city; ?>
		<?php print $saf_current_city; ?>
	</div>
	
	<div id="state">
		<?php print $label_saf_current_state; ?>
		<?php print $saf_current_state; ?>
	</div>
	
	<?php print $label_saf_current_zip; ?>
	<?php print $saf_current_zip; ?>
		
	</div>
	
	
	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_your_business_view.php'); ?>	

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_contact_info_view.php'); ?>

	<div class="submit_button showable"><?php print $saf_btnsubmit?></div>

</div>
</div>
