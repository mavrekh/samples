<div class="saf-outer">
	<div id="container" class="no_sidebar">
	<h2>Prototype / Design Assistance</h2>

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_select_industry_view.php'); ?>	

	<div class="showable">
	
	<?php print $label_saf_proto_product_idea; ?>
	<?php print $saf_proto_product_idea; ?>


	<div id="filler">
	</div>

	<?php print $label_saf_proto_needs; ?>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_CAD; ?>
	<?php print $label_saf_proto_needs_CAD; ?>
	</span>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_Rapid_saf_prototyping; ?>
	<?php print $label_saf_proto_needs_Rapid_saf_prototyping; ?>
	</span>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_Sample_Making; ?>
	<?php print $label_saf_proto_needs_Sample_Making; ?>
	</span>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_Pattern_Making; ?>
	<?php print $label_saf_proto_needs_Pattern_Making; ?>
	</span>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_Casting; ?>
	<?php print $label_saf_proto_needs_Casting; ?>
	</span>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_Metal_Fabrication; ?>
	<?php print $label_saf_proto_needs_Metal_Fabrication; ?>
	</span>

	<span class="checkbox-unit">
	<?php print $saf_proto_needs_Other_Product_Design; ?>
	<?php print $label_saf_proto_needs_Other_Product_Design; ?>
	</span>
	</div>

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_your_business_view.php'); ?>	

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_contact_info_view.php'); ?>

	<div class="submit_button showable"><?php print $saf_btnsubmit?></div>
	
</div>
</div>