<div class="saf-outer">
	<div id="container" class="no_sidebar">
	<h1>Manufacturing</h1>

	<h3>Get help finding the right manufacturer</h3>

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_select_industry_view.php'); ?>	

	<div class="showable">
		<?php print $label_saf_manuf_has_sample; ?>

		<span class="checkbox-unit">
			<?php print $saf_manuf_has_sample_yes; ?>
			<?php print $label_saf_manuf_has_sample_yes; ?>
		</span>

		<span class="checkbox-unit">
			<?php print $saf_manuf_has_sample_no; ?>
			<?php print $label_saf_manuf_has_sample_no; ?>
		</span>
	</div>
	
	<div class="optional toggle_sample_yes">

		<?php print $label_saf_manuf_working_with_manufacturer; ?>

		<span class="checkbox-unit">
			<?php print $saf_manuf_working_with_manufacturer_yes; ?>
			<?php print $label_saf_manuf_working_with_manufacturer_yes; ?>
		</span>
		
		<span class="checkbox-unit">
			<?php print $saf_manuf_working_with_manufacturer_no; ?>
			<?php print $label_saf_manuf_working_with_manufacturer_no; ?>
		</span>

		<div class="optional toggle_withmanuf_yes">
			<?php print $label_saf_manuf_name; ?>
			<?php print $saf_manuf_name; ?>
		
			<?php print $label_saf_manuf_address; ?>
			<?php print $saf_manuf_address; ?>
		
			<div id="city">
				<?php print $label_saf_manuf_address_city; ?>
				<?php print $saf_manuf_address_city; ?>
			</div>
			
			<div id="state">
				<?php print $label_saf_manuf_address_state; ?>
				<?php print $saf_manuf_address_state; ?>
			</div>
			
			<?php print $label_saf_manuf_address_zip; ?>
			<?php print $saf_manuf_address_zip; ?>
		</div>

		<?php print $label_saf_manuf_product_desc; ?>
		<?php print $saf_manuf_product_desc; ?>

		<?php print $label_saf_manuf_list_materials; ?>
		<?php print $saf_manuf_list_materials; ?>


		<?php print $label_saf_manuf_list_equipment; ?>
		<?php print $saf_manuf_list_equipment; ?>

		<?php print $label_saf_manuf_quantities; ?>
		<?php print $saf_manuf_quantities; ?>

		<?php include(get_template_directory() . '/inc/views/sourcing_assistance_your_business_view.php'); ?>	

		<?php include(get_template_directory() . '/inc/views/sourcing_assistance_contact_info_view.php'); ?>

		<div class="submit_button"><?php print $saf_btnsubmit?>
	</div>

</div>

	<div class="optional toggle_sample_no">
		<h2 class="required">Required!</h2>
		<a href="../sourcing-assistance-product-designprototyping/">Get Product Design/Prototyping Assistance</a>
	</div>

</div>
</div>