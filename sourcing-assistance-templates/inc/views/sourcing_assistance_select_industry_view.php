<style>
.showable {
	display: none;
}
</style>

	<h2 id="industry_pick_header" class="hideable">
		<img src="<?php print get_template_directory_uri(); ?>/images/arrow-red.gif">
		Select an Industry
	</h2>
	
	<div id="industry_pick" class="hideable">
		<span class="checkbox-unit">
			<?php print $saf_industry_Construction; ?>
			<?php print $label_saf_industry_Construction; ?>
		</span>
	
		<span class="checkbox-unit">
			<?php print $saf_industry_Interiors; ?>
			<?php print $label_saf_industry_Interiors; ?>
		</span>

		<span class="checkbox-unit">
			<?php print $saf_industry_Apparel_and_Jewelry; ?>
			<?php print $label_saf_industry_Apparel_and_Jewelry; ?>
		</span>

		<span class="checkbox-unit">
			<?php print $saf_industry_Food; ?>
			<?php print $label_saf_industry_Food; ?>
		</span>

		<span class="checkbox-unit">
			<?php print $saf_industry_Machinery_and_Metals; ?>
			<?php print $label_saf_industry_Machinery_and_Metals; ?>
		</span>

		<span class="checkbox-unit">
			<?php print $saf_industry_Printing; ?>
			<?php print $label_saf_industry_Printing; ?>
		</span>

		<span class="checkbox-unit">
			<?php print $saf_industry_Other; ?>
			<?php print $label_saf_industry_Other; ?>
		</span>
	</div>

	<div id="next_button" class="hideable">
		<div class="submit_button"><?php print $saf_nextbutton; ?></div>
	</div>

	<div id="picked_industry" class="showable">
		<img src="<?php print get_template_directory_uri(); ?>/images/check-red.gif">
		<h3></h3>
	</div>
