
<h3 id="saf_contact_info_heading" class="showable">Your Contact Information</h3>

<div id="saf_contact_info" class="showable">

	<?php print $label_saf_contact_info_name; ?>
	<?php print $saf_contact_info_name; ?>

	<?php print $label_saf_contact_info_email; ?>
	<?php print $saf_contact_info_email; ?>
	
	<?php print $label_saf_contact_info_title; ?>
	<?php print $saf_contact_info_title; ?>	

	<?php print $label_saf_contact_info_business_name; ?>
	<?php print $saf_contact_info_business_name; ?>

	<?php print $label_saf_contact_info_address; ?>
	<?php print $saf_contact_info_address; ?>

	<div id="city">
	<?php print $label_saf_contact_info_city; ?>
	<?php print $saf_contact_info_city; ?>
	</div>
	
	<div id="state">
	<?php print $label_saf_contact_info_state; ?>
	<?php print $saf_contact_info_state; ?>
	</div>
	
	<?php print $label_saf_contact_info_zip; ?>
	<?php print $saf_contact_info_zip; ?>

</div>

<div id="saf_contact_info_footer" class="showable">
If everything looks correct take the final step and submit your request now.
</div>