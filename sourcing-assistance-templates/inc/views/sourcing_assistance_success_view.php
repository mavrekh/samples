<div class="saf-outer">
<div id="container" class="no_sidebar">

	<h2>Congratulations!</h2>

	<div id="success_message">
	Your request has been submitted and our team will review it and get back to you soon.
	In the meantime, if you would like to submit another request in another category <a href="../sourcing-assistance-index/">feel free to do so.</a>
	</div>

</div>
</div>
