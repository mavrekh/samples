<div class="saf-outer">
	<div id="container" class="no_sidebar">

<h2>Material</h2>

<?php include(get_template_directory() . '/inc/views/sourcing_assistance_select_industry_view.php'); ?>	

<div class="showable">
	<?php print $label_saf_source_exchange; ?>
	<?php print $saf_source_exchange_exchange; ?>
	<?php print $label_saf_source_exchange_exchange; ?>
	<?php print $saf_source_exchange_source; ?>
	<?php print $label_saf_source_exchange_source; ?>
</div>

<div class="optional toggle_exchange">
	
	<?php print $label_saf_exch_product_desc; ?>
	<?php print $saf_exch_product_desc; ?>
	
	<?php print $label_saf_material_used; ?>
	<?php print $saf_material_used; ?>
	
	<?php print $label_saf_why_exchange; ?>
	<?php print $saf_why_exchange; ?>

	
	<?php print $label_saf_mat_parameters; ?>
	<?php print $saf_mat_parameters; ?>
	
</div>	




<div class="optional toggle_source">
	
	<?php print $label_saf_source_product_desc; ?>
	<?php print $saf_source_product_desc; ?>
	
		
	<?php print $label_saf_material_sourced; ?>
	<?php print $saf_material_sourced; ?>
	
	
	
	<?php print $label_saf_source_sample; ?>
	
	<span class="checkbox-unit">
		<?php print $saf_source_sample_yes; ?>
		<?php print $label_saf_source_sample_yes; ?>
	</span>

	<span class="checkbox-unit">
		<?php print $saf_source_sample_no; ?>
		<?php print $label_saf_source_sample_no; ?>
	</span>
	

	<?php print $label_saf_source_in_prod; ?>
	
	<span class="checkbox-unit">
		<?php print $saf_source_in_prod_yes; ?>
		<?php print $label_saf_source_in_prod_yes; ?>
	</span>
	
	<span class="checkbox-unit">
		<?php print $saf_source_in_prod_no; ?>
		<?php print $label_saf_source_in_prod_no; ?>
	</span>
	
</div>	

<?php include(get_template_directory() . '/inc/views/sourcing_assistance_your_business_view.php'); ?>	

<?php include(get_template_directory() . '/inc/views/sourcing_assistance_contact_info_view.php'); ?>

<div class="submit_button showable"><?php print $saf_btnsubmit?></div>

</div>
</div>



