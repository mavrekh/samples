<div class="saf-outer">
	<div id="container" class="no_sidebar">

	<h2>Need Something Else?</h2>

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_select_industry_view.php'); ?>	

	<div class="showable">
		<?php print $label_saf_other_needs; ?>
		<?php print $saf_other_needs; ?>
	</div>
	
	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_your_business_view.php'); ?>	

	<?php include(get_template_directory() . '/inc/views/sourcing_assistance_contact_info_view.php'); ?>

	<div class="submit_button showable"><?php print $saf_btnsubmit?></div>

</div>
</div>