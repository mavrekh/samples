<style>
.no_sidebar {
         padding-left: 0px;
}
</style>

<script>
$(document).ready(function(){
	$(".saf_index_item").hover(function(){
		$('.ballimg', this).attr("src", function(index, attr){
			return attr.replace("-grey-circle.png", "-red-circle.png");
		});
	}, function(){
		$('.ballimg', this).attr("src", function(index, attr){
			return attr.replace("-red-circle.png", "-grey-circle.png");
		});
	});
});

</script>

<div class="saf-outer">
	<div id="container" class="no_sidebar">

	<div id="saf_index">
	
	<div class="header">
		<h1>Sourcing Assistance</h1>
		<span>Find the perfect manufacturer or local component for your product or design</span>
	</div>

	<ul id="saf_index_list">

		<li>
			<div class="saf_index_item">
				<a href="../sourcing-assistance-manufacturing/">
				<div class="saf_ball_and_chain">
					<img class="ballimg" src="<?php print get_template_directory_uri(); ?>/images/sourcing-grey-circle.png" >
					<h2>Manufacturing</h2>
				</div>
				<h4>Get help finding the right manufacturer.</h4>
				</a>
			</div>
		</li>

		<li>
			<div class="saf_index_item">
				<a href="../sourcing-assistance-materials/">
				<div class="saf_ball_and_chain">
					<img class="ballimg" src="<?php print get_template_directory_uri(); ?>/images/sourcing-grey-circle.png"  >
					<h2>Materials</h2>
				</div>
				<h4>Exchange a material or source material for production.</h4>
				</a>
			</div>
		</li>

		<li>
			<div class="saf_index_item">
				<a href="../sourcing-assistance-product-designprototyping/">
				<div class="saf_ball_and_chain">
					<img class="ballimg" src="<?php print get_template_directory_uri(); ?>/images/sourcing-grey-circle.png"  >
					<h2>Product Design/Prototyping</h2>
				</div>
				<h4>Get help finding the right design source.</h4>
				</a>
			</div>
		</li>

		<li>
			<div class="saf_index_item">
				<a href="../sourcing-assistance-space/">
				<div class="saf_ball_and_chain">
					<img class="ballimg" src="<?php print get_template_directory_uri(); ?>/images/sourcing-grey-circle.png"  >
					<h2>Space</h2>
				</div>
				<h4>Get help finding the right space.</h4>
				</a>
			</div>
		</li>

		<li>
			<div class="saf_index_item">
				<a href="../sourcing-assistance-need-something-else/">
				<div class="saf_ball_and_chain">
					<img class="ballimg" src="<?php print get_template_directory_uri(); ?>/images/sourcing-grey-circle.png"  >
					<h2>Need Something Else?</h2>
				</div>
				<h4>Get help with your other sourcing needs.</h4>
				</a>
			</div>
		</li>


	</ul>



	</div>

</div>
</div>