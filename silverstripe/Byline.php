<?php
/**
 * Defines the Byline Data Object
 */
class Byline extends DataObject {
	static $db = array(
		'Title' => 'Varchar(255)',
		'Date' => 'Date',
		'Description' => 'Varchar(255)',
		'URL' => 'Varchar(255)',
	);

	static $has_one = array(
		'Person' => 'Person',
		'PublicationLogo' => 'PublicationLogo'
	);

	function getCMSValidator(){
		return new Byline_Validator();
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();
 
 		$fields->addFieldToTab('Root.Main', new TextField('Title', 'Title'));
 		$fields->addFieldToTab('Root.Main', new TextField('URL', 'Full URL to the work.  <em>If you need to upload a pdf because the work is behind a paywall or unavailable online contact the adminstrator.</em>'));

 		$fields->addFieldToTab('Root.Main', new TextField('Description','Description'));
 
		$fields->addFieldToTab('Root.Main', new DatePickerField('Date','Select a date'));

		// ModelAdmin pulls up Person as a useless drop down so we have to get rid of it

		$fields->removeFieldFromTab('Root.Main', 'Person');

		$person_HODOM = new HasOneDataObjectManager(
			$this,
			'Person',
			'Person',
			array(
				'LastName' => 'LastName',
				'FirstName' => 'First Name',
			),
			'getCMSFields_forPopup'
		);

		$person_HODOM->setPermissions(array());

		$fields->addFieldToTab('Root.Main', $person_HODOM );

		// ModelAdmin pulls up Publication Logo as a useless drop down so we have to get rid of it

		$fields->removeFieldFromTab('Root.Main', 'Publication Logo');

		$publogo_HODOM = new HasOneDataObjectManager(
			$this,
			'PublicationLogo',
			'PublicationLogo',
			array(
				'Title' => 'Title',
			),
			'getCMSFields_forPopup'
		);

		$publogo_HODOM->setPermissions(array());

		$fields->addFieldToTab('Root.Main', $publogo_HODOM );

		return $fields;
	}

	function canCreate() {return true;} 
	function canEdit() {return true;} 
	
} // Byline
 
class Byline_Validator extends Validator{
	function javascript(){
		return false;
	}

	function php($data){
		$bRet = true;

		if (!$data['Title'] || strlen($data['Title']) > 100){
			$this->validationError(
				'Title',
				'Title is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}


		if (!$data['Date']){
			$this->validationError(
				'Date',
				'Date is Required',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Description'] || strlen($data['Description']) > 250){
			$this->validationError(
				'Description',
				'Description is Either Missing or Longer than 250 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['URL'] || strlen($data['URL']) > 250){

/*
error_log('hello', 0);

if(filter_var('http://www.hello.com', FILTER_VALIDATE_URL,FILTER_FLAG_QUERY_REQUIRED)){
error_log('is a url', 0);
}else {
error_log('is NOT a url', 0);
}
*/

		
			$this->validationError(
				'URL',
				'URL is Either Missing or Longer than 250 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Person'][0]){
			$this->validationError(
				'Form_AddForm_Person_CheckedList',
				'Person is Required',
				"required"
			);

			$bRet = false;
		}

		if (!$data['PublicationLogo'][0]){
			$this->validationError(
				'Form_AddForm_PublicationLogo_CheckedList',
				'PublicationLogo is Required',
				"required"
			);

			$bRet = false;
		}

		return $bRet;
	}
}
 
?>
