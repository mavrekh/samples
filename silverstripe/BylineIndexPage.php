<?php
/**
 * Defines the BylineIndexPage page type
 * We don't need a validator for this since we have a single field that is a 
 * drop-down menu.
 */
class BylineIndexPage extends Page {
	static $db = array(
		'Type' => 'Varchar(255)',
	);
	static $has_one = array();

	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Type', 'Type', array(
				'All' => 'All', 
				'Student' => 'Student', 
				'Alumni' => 'Alumni', 
				'Faculty' => 'Faculty',
			)), 'Content'
		);

		return $fields;
	}
}
 
class BylineIndexPage_Controller extends Page_Controller {

        public function init() {
                RSSFeed::linkToFeed($this->Link("rss"), "Arthur L. Carter Journalism Institute Bylines RSS Feed");
                parent::init();
        }

        public function rss() {
                $rss = new RSSFeed($this->getPagedBylines(), $this->Link("rss"), "Arthur L. Carter Journalism Institute Bylines");
                return $rss->outputToBrowser();
        }


	function getPagedBylines() {

		// returns paginated bylines - can be filtered according to author type, 
		// ie faculty, alumni, student or all

		if(!isset($_GET['start']) || !is_numeric($_GET['start']) || (int)$_GET['start'] < 1){
			$_GET['start'] = 0;
		}
		
		$SQL_start = (int)$_GET['start'];

		// set our filter based on what Type we have selected for the page

		if(!$this->Type || $this->Type == 'All'){
			$person_filter = "";
		} else {
			$person_filter = "`Person`.`Type` = '".$this->Type."'";
		}

		// get the data objects, including person and publication logo

		$Bylines = DataObject::get(
			$callerClass = "Byline",
			$filter = $person_filter,
			$sort = "Date DESC",
			$join = "
				LEFT JOIN `Person` ON `Person`.`ID` = `Byline`.`PersonID`
				LEFT JOIN `PublicationLogo` ON `PublicationLogo`.`ID` = `Byline`.`PublicationLogoID`
			",
			$limit = "{$SQL_start},10"
		);

		return $Bylines ? $Bylines : false;

	} // getPagedBylines
}

?>
