<?php

class UnitEventHolder extends EventHolder {
   
}

class UnitEventHolder_Controller extends EventHolder_Controller {
	public $unitEventHolder = true;

	function getFutureEvents(){
		// set $now to actually be yesterday so today event show up
		$now = date('Y-m-d H:i:s', strtotime("-1 day"));

		if(!isset($_GET['start']) || !is_numeric($_GET['start']) || (int)$_GET['start'] < 1){
			$_GET['start'] = 0;
		}
		
		$SQL_start = (int)$_GET['start'];

		$Events = DataObject::get(
			$callerClass = "EventPage",
			$filter = "((Date > '$now') OR (EndDate is not null AND (EndDate > '$now'))) and (`EventPage_Units`.`UnitID`= '" . $this->Parent->UnitID . "')",
			$sort = "Date ASC",
			$join = "
				JOIN `EventPage_Units` ON `EventPage`.`ID` = `EventPage_Units`.`EventPageID`
			",
			$limit = "{$SQL_start}, 15"
		);

		return $Events ? $Events : false;
	}


	function getPastEvents(){
		if(!isset($_GET['start']) || !is_numeric($_GET['start']) || (int)$_GET['start'] < 1){
			$_GET['start'] = 0;
		}
		
		// set $now to actually be tomorrow so today event don't show up
		$now = date('Y-m-d H:i:s', strtotime("-1 day"));
		
		$SQL_start = (int)$_GET['start'];

		$Events = DataObject::get(
			$callerClass = "EventPage",
			$filter = "Date < '$now' AND (EndDate is null OR (EndDate < '$now')) and `EventPage_Units`.`UnitID`= '" . $this->Parent->UnitID . "'",
			$sort = "Date DESC",
			$join = "
				JOIN `EventPage_Units` ON `EventPage`.`ID` = `EventPage_Units`.`EventPageID`
			",
			$limit = "{$SQL_start}, 15"
		);

		return $Events ? $Events : false;
	}

}


?>
