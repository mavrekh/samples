<?php
/**
 * Defines the UnitStudentWork page type
 */
class UnitStudentWork extends Page {
	static $db = array();
	
	static $has_one = array(
		'Unit' => 'Unit'
	);

	function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$tablefield_unit = new HasOneDataObjectManager(
			$this,
			'Unit',
			'Unit',
			array('Name' => 'Unit Name'),
			'getCMSFields_forPopup'
		);

		$tablefield_unit->setPermissions(array());

		/*
		*	since we only need to add units ONCE to the main unit and the fauclty unit page
		*	we will remove this field so it cannot be changed once we go to production
		*
		*/

		$fields->addFieldToTab('Root.Content.Unit', $tablefield_unit );
		
		return $fields;
	}
}
 
class UnitStudentWork_Controller extends Page_Controller {

	function getPagedBylines() {
		// returns paginated student and alumni bylines
		
		if(!isset($_GET['start']) || !is_numeric($_GET['start']) || (int)$_GET['start'] < 1){
			$_GET['start'] = 0;
		}
		
		$SQL_start = (int)$_GET['start'];

		// get the data objects, including person and publication logo

		$Bylines = DataObject::get(
			$callerClass = "Byline",
			$filter = "(`Person`.`Type` = 'student' OR `Person`.`Type` = 'alumni') AND `Person_Units`.`UnitID` = '".$this->UnitID."'",
			$sort = "Date DESC",
			$join = "
				LEFT JOIN `Person` ON `Person`.`ID` = `Byline`.`PersonID`
				LEFT JOIN `PublicationLogo` ON `PublicationLogo`.`ID` = `Byline`.`PublicationLogoID`
				LEFT JOIN `Person_Units` ON `Person_Units`.`PersonID` = `Person`.`ID`
			",
			$limit = "{$SQL_start},10"
		);

		return $Bylines ? $Bylines : false;

	} // getPagedBylines
}

?>