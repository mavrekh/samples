<?php

class Media extends DataObject {
 
	static $db = array(
		'Title' => 'Varchar(255)',
		'Type' => 'Varchar(255)',
		'VideoID' => 'Varchar(255)',
		'Description' => 'Varchar(255)',
	);

	static $has_one = array(
    	'Image' => 'Image',
    );

	static $default_sort = 'Title ASC';

	static $searchable_fields = array(
		'Title',
		'Type',
		'Description'
	);

	function getCMSValidator(){
		return new Media_Validator();
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Main', new TextField('Title', 'Title'));

		$fields->addFieldToTab('Root.Main', new TextField('Description', 'Description'));

 		$fields->addFieldToTab('Root.Main', new DropdownField('Type', 'Type', array(
				'Image' => 'Image', 
				'Vimeo' => 'Vimeo', 
				'YouTube' => 'YouTube', 
			))
		);

		$fields->addFieldToTab('Root.Main', new TextField('VideoID', 'VideoID'));
		
 		$fields->addFieldToTab('Root.Main', new ImageField('Image', 'Image', '', '', '', $folderName = 'Media/'));

		return $fields;
	}	

}

class Media_Validator extends Validator{
	/*
	* this is the class we use for validation
	* we don't need to validate the following because they are drop menus
	* Type
	* if video is selected we make sure there is a vimeo id
	* however, because of how SS works, we can't validate for image because the object has to 
	* exists before we can attach it - catch 22...
	*/

	function javascript(){
		return false;
	}

	// core requirements

	function php($data){
		$bRet = true;

		if (!$data['Title'] || strlen($data['Title']) > 200){
			$this->validationError(
				'Title',
				'Title is Either Missing or Longer than 200 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Description'] || strlen($data['Description']) > 200){
			$this->validationError(
				'Description',
				'Description is Either Missing or Longer than 200 Characters',
				"required"
			);

			$bRet = false;
		}

		// if the type is video we make sure there is a vimeo id
	
		if(($data['Type'] == 'Vimeo' || $data['Type'] == 'YouTube') && !$data['VideoID']){
			$this->validationError(
				'VimeoID',
				'VimeoID is Missing',
				"required"
			);

			$bRet = false;
		}

		return $bRet;
	}
}

?>
