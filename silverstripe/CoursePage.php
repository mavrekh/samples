<?php
/**
 * Defines the CoursePage page type
 */
class CoursePage extends Page {

	static $db = array(
		'Semester' => 'Varchar(255)',
		'Year' => 'Varchar(255)',
		'Type' => 'Varchar(255)',
		'CourseID' => 'Varchar(255)',
		'Days' => 'Varchar(255)',
		'Location' => 'Varchar(255)',
		'Capstone' => 'Varchar(255)'
	);

	static $has_one = array(
		'Syllabus' => 'File',
		'CourseLogo' => 'Image'
	);

	static $many_many = array (
		'People' => 'Person',
		'Units' => 'Unit',
	);

	function getCMSValidator(){
		return new CoursePage_Validator();
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();
 
 		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Semester', 'Semester', array(
				'Spring' => 'Spring', 
//				'Summer' => 'Summer',
				'Fall' => 'Fall', 
				'Summer1' => 'Summer 1',
				'Summer2' => 'Summer 2',
			)), 'Content'
		);
 
  		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Year', 'Year', array(
				'2014' => '2014',
				'2013' => '2013',
				'2012' => '2012',
				'2011' => '2011',
				'2010' => '2010',
				'2010' => '2010',
				'2009' => '2009',
				'2008' => '2008',
				'2007' => '2007',
				'2006' => '2006',
				'2005' => '2005',
				'2004' => '2004',
				'2003' => '2003',
				'2002' => '2002',
				'2001' => '2001',
				'2000' => '2000',
			)), 'Content'
		);
 
   		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Type', 'Type', array(
   				'Undergraduate Required' => 'Undergraduate Required',
   				'Undergraduate Elective' => 'Undergraduate Elective',
				'Elective Course' => 'Elective Course',
				'First Semester Course' => 'First Semester Course',
				'Second Semester Course' => 'Second Semester Course',
				'Third Semester Course' => 'Third Semester Course',	
				'Pre-college' => 'Pre-college',	
			)), 'Content'
		);	
 
 		$fields->addFieldToTab('Root.Content.Main', new TextField('CourseID', 'CourseID'), 'Content');

 		$fields->addFieldToTab('Root.Content.Main', new TextField('Days', 'Days and Times'), 'Content');
 		
 		$fields->addFieldToTab('Root.Content.Main', new TextField('Location', 'Location'), 'Content');

   		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Capstone', 'Is this a Captsone Course?', array(
				'No' => 'No',
				'Yes' => 'Yes',
			)), 'Content'
		);				

 		/*
		* this fields stores our syllabus
		* we use the Year and semester to place it in the appropriate directory.
		* to make sure the correct paths are set we only show the Location has been set as this presumes
		* that the other information has been entered as well.  bit hackish, possibility for rewrite
		*************
 		*/
		if($this->Year && $this->Semester){
			$fields->addFieldToTab('Root.Content.Main', new FileIFrameField('Syllabus', 'Syllabus', '', '', '', $folderName = 'Syllabi/'.$this->Year.'/'.$this->Semester), 'Content');
		}

		$fields->addFieldToTab('Root.Content.Main', new ImageField('CourseLogo', 'Course Logo (optional)', '', '', '', $folderName = 'Courses/Logos'), 'Content');

		$tablefield_person = new ManyManyDataObjectManager(
			$this,
			'People',
			'Person',
			array(
				'FirstName' => 'First Name',
				'LastName' => 'LastName',
				'Type' => 'Type',
			),
			'',
			"`Type` = 'Faculty'",
			'LastName ASC'
		);

		$tablefield_person->setPermissions(array());

		$fields->addFieldToTab('Root.Content.Instructors', $tablefield_person );

		$unitsMMDOM = new ManyManyDataObjectManager(
			$this,
			'Units',
			'Unit',
			array('Name' => 'Unit Name')
		);

		// we don't want people adding new units!
		$unitsMMDOM->setPermissions(array());

		$fields->addFieldToTab('Root.Content.Units', $unitsMMDOM);

		return $fields;

	}

}

class CoursePage_Controller extends Page_Controller {

 	function getCourseFacultyPage() { 	

		$FacultyPages = new DataObjectSet();

		foreach($this->People() as $Person){
			if($FacultyPage = DataObject::get_one('FacultyPage', 'PersonID ='.$Person->ID)){
				$FacultyPages->push($FacultyPage);
			}
		}
		return $FacultyPages;
 	}// getCourseFacultyPages

	function getCourseUnitPage() {
		$PersonUnits = new DataObjectSet();

		foreach($this->Units() as $Unit){
			if($UnitPage = DataObject::get_one('UnitHomePage', 'UnitID = '.$Unit->ID)){
				$PersonUnits->push($UnitPage);
			}
		}
		return $PersonUnits;
	}//getPersonUnits
}
 
class CoursePage_Validator extends Validator{
	/*
	* this is the class we use for validation
	* we don't need to validate the following because they are drop menus
	* Semester, Year, Type, Captsone
	*/


	function javascript(){
		return false;
	}


	function php($data){
		$bRet = true;

		if (!$data['Title'] || strlen($data['Title']) > 100){
			$this->validationError(
				'Form_EditForm_Title',
				'Title is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['MenuTitle'] || strlen($data['MenuTitle']) > 100){
			$this->validationError(
				'Form_EditForm_MenuTitle',
				'MenuTitle is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Content']){
			$this->validationError(
				'Form_EditForm_Content',
				'Content is Missing',
				"required"
			);

			$bRet = false;
		}

		if (!$data['CourseID'] || strlen($data['CourseID']) > 100){
			$this->validationError(
				'Form_EditForm_CourseID',
				'CourseID is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}
		
		if (!$data['Days'] || strlen($data['Days']) > 100){
			$this->validationError(
				'Form_EditForm_Days',
				'Days is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}
		
		if (!$data['Location'] || strlen($data['Location']) > 100){
			$this->validationError(
				'Form_EditForm_Location',
				'Location is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		/*
		
		Syllabus is NOT REQUIRED
		
		if (!RelationshipValidator::has_file('CoursePage', $data['ID'], 'SyllabusID')){
			$this->validationError(
				'Form_EditForm_Syllabus',
				'We need a Syllabus',
				"required"
			);

			$bRet = false;
		}

		ISSUES WITH VALIDATION B/C OF MULITIPLE PAGES OF PEOPLE, SO REMOVING

		if (!$data['People'][0]){
			$this->validationError(
				'Content',
				'We Need Instructors',
				"required"
			);

			$bRet = false;
		}
		*/
		if (!$data['Units'][0]){
			$this->validationError(
				'Content',
				'We Need Units',
				"required"
			);

			$bRet = false;
		}

		return $bRet;
	}
}
 
?>
