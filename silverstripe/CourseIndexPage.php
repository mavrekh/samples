<?php
/**
 * Defines the CourseIndexPage page type
 * We do no validation since the only two fields are drop-down menus.
 */
class CourseIndexPage extends Page {
	static $db = array(
		'Semester' => 'Varchar(255)',
		'Year' => 'Varchar(255)',
	);
	static $has_one = array();

	function getCMSFields() {
		$fields = parent::getCMSFields();

 		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Semester', 'Semester', array(
				'Spring' => 'Spring', 
	//			'Summer' => 'Summer', 
				'Fall' => 'Fall', 
				'Summer1' => 'Summer 1',
				'Summer2' => 'Summer 2',
			)), 'Content'
		);
 
  		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Year', 'Year', array(
				'2014' => '2014',
				'2013' => '2013',
				'2012' => '2012',
				'2011' => '2011',
				'2010' => '2010',
				'2010' => '2010',
				'2009' => '2009',
				'2008' => '2008',
				'2007' => '2007',
				'2006' => '2006',
				'2005' => '2005',
				'2004' => '2004',
				'2003' => '2003',
				'2002' => '2002',
				'2001' => '2001',
				'2000' => '2000',
			)), 'Content'
		);

		return $fields;

	}

}

class CourseIndexPage_Controller extends Page_Controller {

	function getCourses_BER(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Business and Economic Reporting'")->ID);
	}

	function getCourses_CRC(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Cultural Reporting and Criticism'")->ID);
	}

	function getCourses_GLOJO(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Global and Joint Program Studies'")->ID);
	}

	function getCourses_LitRep(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Literary Reportage'")->ID);
	}

	function getCourses_Magazine(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Magazine Writing'")->ID);
	}
	
	function getCourses_NewsDoc(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'News and Documentary'")->ID);
	}

	function getCourses_RNY(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Reporting New York'")->ID);
	}
	
	function getCourses_RTN(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Reporting the Nation'")->ID);
	}

	function getCourses_S20(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Studio 20'")->ID);
	}

	function getCourses_SHERP(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Science, Health and Environmental Reporting'")->ID);
	}

	function getCourses_UGMC(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Media Criticism'")->ID);
	}
	
	function getCourses_UGJ(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Journalism'")->ID);
	}

	function getCourses_Precollege(){
		return $this->getCourses(DataObject::get_one('Unit', "Name = 'Pre-college'")->ID);
	}

	function getCourses($UnitID) {

		$CoursesAllData = new DataObjectSet();

		$Courses = DataObject::get(
			$callerClass = "CoursePage",
			$filter = "
				`CoursePage`.`Semester` LIKE '".$this->Semester."'
				AND
				`CoursePage`.`Year` LIKE '".$this->Year."'
				AND
				`CoursePage_Units`.`UnitID` = '".$UnitID."'
				AND
				`CoursePage`.`Type` != 'Elective Course'
				",
			$sort = "`CoursePage`.`Type` ASC",
			$join = "
				LEFT JOIN `CoursePage_Units` ON `CoursePage_Units`.`CoursePageID` = `CoursePage`.`ID`			
			",
			$limit = ""
		);

		foreach($Courses as $Course){
		
			$CoursesAllData->push(new ArrayData(array(
				'Course' => $Course,
				'FacultyPages' => $this->getCourseFacultyPages($Course)
			)));
		}

		return $CoursesAllData ? $CoursesAllData : false;
	} // getCourses

 	function getCourseFacultyPages($Course) { 	

		$FacultyPages = new DataObjectSet();

		foreach($Course->People() as $Person){
			if($FacultyPage = DataObject::get_one('FacultyPage', 'PersonID ='.$Person->ID)){
				$FacultyPages->push($FacultyPage);
			}
		}
		return $FacultyPages;
 	}// getCourseFacultyPages

	function getElectiveCourses() {
		$CoursesAllData = new DataObjectSet();

		$Courses = DataObject::get(
			$callerClass = "CoursePage",
			$filter = "
				`CoursePage`.`Semester` LIKE '".$this->Semester."'
				AND
				`CoursePage`.`Year` LIKE '".$this->Year."'
				AND
				`CoursePage`.`Type` LIKE 'Elective Course'
				",
			$sort = "`CoursePage`.`Type` ASC",
			$join = "
				LEFT JOIN `CoursePage_Units` ON `CoursePage_Units`.`CoursePageID` = `CoursePage`.`ID`			
			",
			$limit = ""
		);

		foreach($Courses as $Course){
		
			$CoursesAllData->push(new ArrayData(array(
				'Course' => $Course,
				'FacultyPages' => $this->getCourseFacultyPages($Course)
			)));
		}

		return $CoursesAllData ? $CoursesAllData : false;
	} // getElectiveCourses
}
 
?>
