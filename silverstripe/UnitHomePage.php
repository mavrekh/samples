<?php
/**
 * Defines the UnitHomePage page type
 */
class UnitHomePage extends Page {

	static $db = array(
		'Tagline' => 'Varchar(255)',
	);
	
	static $has_one = array(
		'Unit' => 'Unit'
	);
   
	static $many_many = array(
		'FeaturedMedia' => 'Media',
	);

	function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->addFieldToTab('Root.Content.Main', new TextField('Tagline', 'Tagline'), 'Content');
		
		$tablefield_unit = new HasOneDataObjectManager(
			$this,
			'Unit',
			'Unit',
			array('Name' => 'Unit Name'),
			'getCMSFields_forPopup'
		);

		$tablefield_unit->setPermissions(array());

		/*
		*	since we only need to add units ONCE to the main unit and the fauclty unit page
		*	we will remove this field so it cannot be changed once we go to production
		*
		*/

//		$fields->addFieldToTab('Root.Content.Unit', $tablefield_unit );

		$tablefield_media = new ManyManyDataObjectManager(
			$this,
			'FeaturedMedia',
			'Media',
			array(
				'Title' => 'Title'
			),
			'',
			'',
			'Title ASC'
		);

		$tablefield_media->setPermissions(array());

		$fields->addFieldToTab('Root.Content.FeaturedMedia', $tablefield_media );

		return $fields;
	}// getCMSFields
} //UnitHomePage

class UnitHomePage_Controller extends Page_Controller {

	public function init() {
		parent::init();
		// get all related people objects when we load the unit page
		$this->People = DataObject::get_by_id('Unit', $this->UnitID)->People();
	}

 	function getUnitFacultyPages() { 	
 		return $this->getUnitPersonPages('FacultyPage');
 	} // getUnitFacultyPages

 	function getUnitStudentPages() { 	
 		return $this->getUnitPersonPages('StudentPage');
 	} // getUnitStudentPages
 
 	function getUnitAlumniPages() { 	
 		return $this->getUnitPersonPages('AlumniPage');
 	} // getUnitAlumniPages
 
 	function getUnitPersonPages($Type) { 	
		$UnitPersonPages = DataObject::get(
			$callerClass = "$Type",
			$filter = "`Person_Units`.`UnitID` = '".$this->UnitID."'",
			$sort = "`Person`.`SpecialSort` DESC, `Person`.`LastName` ASC",
			$join = "
				LEFT JOIN `Person` ON `Person`.`ID` = `$Type`.`PersonID`
				LEFT JOIN `Person_Units` ON `Person_Units`.`PersonID` = `Person`.`ID`
			",
			$limit = ""
		);

		return $UnitPersonPages ? $UnitPersonPages : false;
 	}// getUnitPersonPages

	function getUnitBylines() {
		$UnitBylines = DataObject::get(
			$callerClass = "Byline",
			$filter = "`Person_Units`.`UnitID` = '".$this->UnitID."'",
			$sort = "Date DESC",
			$join = "
				LEFT JOIN `Person` ON `Person`.`ID` = `Byline`.`PersonID`
				LEFT JOIN `Person_Units` ON `Person_Units`.`PersonID` = `Person`.`ID`
			",
			$limit = "16"
		);

		return $UnitBylines ? $UnitBylines : false;
		
	}// getUnitBylines
	
	function getUnitFutureEvents(){
		// get 3 future events

		$now = date('Y-m-d');

		$futureEvents = DataObject::get(
			$callerClass = "EventPage",
			$filter ="`EventPage`.`Date` >= '$now' AND `EventPage_Units`.`UnitID` = '".$this->UnitID."'",
			$sort = "`EventPage`.`Date` ASC",
			$join = "
				LEFT JOIN `EventPage_Units` ON `EventPage_Units`.`EventPageID` = `EventPage`.`ID`
			",
			$limit = "3"
		);
		
		return $futureEvents ? $futureEvents : false;
		
	}// getUnitFutureEvents

	function getUnitPastEvents(){

		// return 3 past Events
		//		

		$now = date('Y-m-d');
	
		$pastEvents = DataObject::get(
			$callerClass = "EventPage",
			$filter ="`EventPage`.`Date` < '$now' AND `EventPage_Units`.`UnitID` = '".$this->UnitID."'",
			$sort = "`EventPage`.`Date` DESC",
			$join = "
				LEFT JOIN `EventPage_Units` ON `EventPage_Units`.`EventPageID` = `EventPage`.`ID`
			",
			$limit = "3"
		);
	
		return $pastEvents ? $pastEvents : false;
		
	}// getUnitUpcomingEvents

	function getUnitCourses(){
		return DataObject::get_by_id('Unit', $this->UnitID)->Courses();
	}// getUnitCourses

}//UnitHomePage_Controller

?>
