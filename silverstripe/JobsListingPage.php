<?php
/**
 * Defines the JobsListingPage page type
 */
class JobsListingPage extends Page {
   static $db = array(
   	'Public' => 'Varchar(1)',
   );
   
   	function getCMSFields() {
		$fields = parent::getCMSFields();
 
 		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Public', 'Public', array(
				'1' => 'Yes',
				'0' => 'No'
			)), 'Content'
		);

		return $fields;
	}
   
   static $has_one = array();
}
 
class JobsListingPage_Controller extends Page_Controller {

	function getJobs(){
		// return jobs that have been approved, newest first
		//
		return DataObject::get('Job', "Approved = 'Accepted'", "Date DESC");
	}


	function getJInternships() {
		return DataObject::get('Job', "ListingType = 'Journalism Internship' and Approved = 'Accepted'", "Date DESC");
	}
	
	function getPaidJInternships() {
		return DataObject::get('Job', "ListingType = 'Paid Journalism Internship' and Approved = 'Accepted'", "Date DESC");
	}

	function getJJobs() {
		return DataObject::get('Job', "ListingType = 'Journalism Job' and Approved = 'Accepted'", "Date DESC");
	}
	
	function getMediaRelatedJobs() {
		return DataObject::get('Job', "ListingType = 'Media-Related Job' and Approved = 'Accepted'", "Date DESC");	
	}
	
	function getContributorJobs() {
		return DataObject::get('Job', "ListingType = 'Contributor' and Approved = 'Accepted'", "Date DESC");		
	}
	
}
 
?>
