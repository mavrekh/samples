<?php
 
class FacultyPage extends Page {
	static $db = array(
		'FacultyType' => 'Varchar(255)',
		'ProfessionalTitle' => 'Varchar(255)',
		'Interests' => 'Varchar(255)',
	    'Email' => 'Varchar(255)',
		'Phone_Office' => 'Varchar(255)',
		'Twitter' => 'Varchar(255)',
		'Facebook' => 'Varchar(255)',
		'Wikipedia' => 'Varchar(255)',
		'Link_1_URL' => 'Varchar(255)',
		'Link_1_Title' => 'Varchar(255)',
		'Link_2_URL' => 'Varchar(255)',
		'Link_2_Title' => 'Varchar(255)',
		'Link_3_URL' => 'Varchar(255)',
		'Link_3_Title' => 'Varchar(255)',
	);

	static $has_one = array(
		'Person' => 'Person',
		'HeadShot' => 'Image'
	);
/*
	function getCMSValidator(){
		return new FacultyPage_Validator();
	}
*/
	function getCMSFields() {
		$fields = parent::getCMSFields();

 		$fields->addFieldToTab('Root.Content.Main', new ImageField('HeadShot', 'HeadShot', '', '', '', $folderName = 'HeadShots/Faculty'), 'Content');
	    $fields->addFieldToTab('Root.Content.Main', new TextField('Email', 'Email'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Phone', 'Office Phone'), 'Content');

		$fields->addFieldToTab('Root.Content.Main', new DropdownField('FacultyType', 'Type of Faculty', array(
				'Professor' => 'Professor', 
				'Professor Emeritus' => 'Professor Emeritus',
				'Associate Professor' => 'Associate Professor', 
				'Assistant Professor' => 'Assistant Professor', 
				'Clinical Professor' => 'Clinical Professor', 
				'Clinical Associate Professor' => 'Clinical Associate Professor', 
				'Clinical Assistant Professor' => 'Clinical Assistant Professor', 
				'Visiting Assistant Professor' => 'Visiting Assistant Professor',
				'Visiting Associate Professor' => 'Visiting Associate Professor',
				'Distinguished Writer in Residence' => 'Distinguished Writer in Residence', 
				'Adjunct Faculty' => 'Adjunct Faculty',
				'Visiting Scholar' => 'Visiting Scholar',
				'Abroad' => 'Abroad'
			)), 'Content'
		);
		
		$fields->addFieldToTab('Root.Content.Main', new TextField('ProfessionalTitle', 'Professional Title'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Interests', 'Interests'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Twitter', 'Twitter URL'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Facebook', 'Facebook URL'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Wikipedia', 'Wikipedia URL'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Link_1_Title', 'Other Link #1 Title'), 'Content');		
		$fields->addFieldToTab('Root.Content.Main', new TextField('Link_1_URL', 'Other Link #1 URL'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Link_2_Title', 'Other Link #2 Title'), 'Content');		
		$fields->addFieldToTab('Root.Content.Main', new TextField('Link_2_URL', 'Other Link #2 URL'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Link_3_Title', 'Other Link #3 Title'), 'Content');		
		$fields->addFieldToTab('Root.Content.Main', new TextField('Link_3_URL', 'Other Link #3 URL'), 'Content');

		$tablefield_person = new HasOneDataObjectManager(
			$this,
			'Person',
			'Person',
			array(
				'FirstName' => 'First Name',
				'LastName' => 'LastName',
				'Type' => 'Type',
			),
			'',
			"`Type` = 'Faculty'",
			'LastName ASC'
		);

		$tablefield_person->setPermissions(array());

		$fields->addFieldToTab('Root.Content.Person', $tablefield_person );

		return $fields;
	}
   
}
 
class FacultyPage_Controller extends PersonPage_Controller {}

class FacultyPage_Validator extends Validator{
	/*
	* this is the class we use for validation
	*/

	function javascript(){
		return false;
	}

	// core requirements

	function php($data){
		$bRet = true;

		if (!$data['Title'] || strlen($data['Title']) > 100){
			$this->validationError(
				'Form_EditForm_Title',
				'Title is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['MenuTitle'] || strlen($data['MenuTitle']) > 100){
			$this->validationError(
				'Form_EditForm_MenuTitle',
				'MenuTitle is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Content']){
			$this->validationError(
				'Form_EditForm_Content',
				'Content is Missing',
				"required"
			);

			$bRet = false;
		}

		// additional requirements

		if (!RelationshipValidator::has_file('FacultyPage', $data['ID'], 'HeadShotID')){
			$this->validationError(
				'Form_EditForm_HeadShot',
				'We need a Headshot.',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Person'][0]){
			$this->validationError(
				'Form_EditForm_Person',
				'We Need a Person',
				"required"
			);

			$bRet = false;
		}

		return $bRet;
	}
}

?>
