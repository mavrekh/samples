<?php
/**
 * Defines the EventHolder page type
 */
class EventHolder extends Page {
	static $db = array(
		'Future' => 'Varchar(100)',
	);
	static $has_one = array();
//	static $allowed_children = array('EventPage');

	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Content.Main', new DropdownField('Future', 'Display Future or Past Events?', array(
				'1' => 'Future', 
				'' => 'Past', 
			)), 'Content'
		);

		return $fields;
	}// getCMSFields


}

class EventHolder_Controller extends Page_Controller {

	public function init() {
		RSSFeed::linkToFeed($this->Link("rss"), "Arthur L. Carter Journalism Institute Events RSS Feed");   
		parent::init();
	}

	public function rss() {
		$rss = new RSSFeed($this->getFutureEvents(), $this->Link("rss"), "Arthur L. Carter Journalism Institute Events");
		return $rss->outputToBrowser();
	}

	public function getFutureEvents(){

		// set $now to actually be yesterday so today event show up
		$now = date('Y-m-d H:i:s', strtotime("-1 day"));

		if(!isset($_GET['start']) || !is_numeric($_GET['start']) || (int)$_GET['start'] < 1){
			$_GET['start'] = 0;
		}
		
		$SQL_start = (int)$_GET['start'];

		$Events = DataObject::get(
			$callerClass = "EventPage",
			$filter = "(Date > '$now') OR (EndDate is not null AND EndDate > '$now')",
			$sort = "Date ASC",
			$join = "",
			$limit = "{$SQL_start}, 15"
		);

		return $Events ? $Events : false;
	}

	function getPastEvents(){
		if(!isset($_GET['start']) || !is_numeric($_GET['start']) || (int)$_GET['start'] < 1){
			$_GET['start'] = 0;
		}
		
		// set $now to actually be tomorrow so today event don't show up
		$now = date('Y-m-d H:i:s', strtotime("-1 day"));
		
		$SQL_start = (int)$_GET['start'];

		$Events = DataObject::get(
			$callerClass = "EventPage",
			$filter = "Date < '$now' AND (EndDate is null OR (EndDate < '$now'))",
			$sort = "Date DESC",
			$join = "",
			$limit = "{$SQL_start}, 15"
		);

		return $Events ? $Events : false;
	}


}
 
?>
