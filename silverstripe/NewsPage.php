<?php
/**
 * Defines the NewsPage page type
 */
class NewsPage extends Page {
	static $db = array(
		'Date' => 'Date',
	);

	static $has_one = array(
		'Image' => 'Image',
	);

	static $many_many = array(
		'FeaturedMedia' => 'Media',
		'Units' => 'Unit',
	);

	function getCMSValidator(){
		return new NewsPage_Validator();
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Content.Main', new DatePickerField('Date','Date'), 'Content');

		$fields->addFieldToTab('Root.Content.Main', new ImageField('Image', 'Image', '', '', '', $folderName = 'News/'), 'Content');

		$unitsMMCTF = new ManyManyComplexTableField(
			$this,
			'Units',
			'Unit',
			array('Name' => 'Unit Name')
		);

		// we don't want people adding new units!
		$unitsMMCTF->setPermissions(array());

		$fields->addFieldToTab('Root.Content.Units', $unitsMMCTF);
	
		$tablefield_media = new ManyManyDataObjectManager(
			$this,
			'FeaturedMedia',
			'Media',
			array(
				'Title' => 'Title'
			),
			'',
			'',
			'Title ASC'
		);

		$tablefield_media->setPermissions(array());

		$fields->addFieldToTab('Root.Content.FeaturedMedia', $tablefield_media );
	
		return $fields;
	}

}
 
class NewsPage_Controller extends Page_Controller {

	function getFeaturedMedia(){
		return $this->FeaturedMedia();
	} //getFeaturedMedia

}

class NewsPage_Validator extends Validator{
	function javascript(){
		return false;
	}

	function php($data){
		$bRet = true;
		
		// core requirements

		if (!$data['Title'] || strlen($data['Title']) > 100){
			$this->validationError(
				'Form_EditForm_Title',
				'Title is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['MenuTitle'] || strlen($data['MenuTitle']) > 100){
			$this->validationError(
				'Form_EditForm_MenuTitle',
				'MenuTitle is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Content']){
			$this->validationError(
				'Form_EditForm_Content',
				'Content is Missing',
				"required"
			);

			$bRet = false;
		}

		// additional requirements
			/*	
		if (!$data['Date']){
			$this->validationError(
				'Form_EditForm_Date',
				'Date is Required',
				"required"
			);

			$bRet = false;
		}

		if (!RelationshipValidator::has_file('NewsPage', $data['ID'], 'ImageID')){
			$this->validationError(
				'Form_EditForm_Image',
				'We need an image',
				"required"
			);

			$bRet = false;
		}
*/
		return $bRet;
	}
}
?>
