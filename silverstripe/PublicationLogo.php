<?php
/**
 * Defines the PublicationLogo Data Object
 */
class PublicationLogo extends DataObject {
	static $db = array(
		'Title' => 'Varchar(255)',
	);

	static $has_one = array(
		'Image' => 'Image'
	);

	function getCMSValidator(){
		return new PublicationLogo_Validator();
	}

	function getCMSFields() {
		$fields = parent::getCMSFields();
 
 		$fields->addFieldToTab('Root.Main', new TextField('Title', 'Title'));

		$fields->addFieldToTab('Root.Main', new ImageField('Image', 'Publication Logo - Dimensions are 100x100', '', '', '', $folderName = 'PublicationLogos'));

		return $fields;
	}

} // PublicationLogo

class PublicationLogo_Validator extends Validator{
	// we'd like to validate for the image, but it's a catch-22
	// since only admin can access this it's not a huge worry

	function javascript(){
		return false;
	}

	function php($data){
		$bRet = true;

		if (!$data['Title'] || strlen($data['Title']) > 100){
			$this->validationError(
				'Title',
				'Title is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		return $bRet;
	}
}

?>
