<?php
/**
 * Defines the HomePage page type
 */
 
class HomePage extends Page {
	static $db = array(
		'NewsTitle1' => 'Varchar(255)',
		'NewsURL1' => 'Varchar(255)',
		'NewsTitle2' => 'Varchar(255)',
		'NewsURL2' => 'Varchar(255)',
		'NewsTitle3' => 'Varchar(255)',
		'NewsURL3' => 'Varchar(255)',
		'NewsTitle4' => 'Varchar(255)',
		'NewsURL4' => 'Varchar(255)',
	);

	static $has_one = array(
		'NewsThumb1' => 'Image',
		'NewsThumb2' => 'Image',
		'NewsThumb3' => 'Image',
		'NewsThumb4' => 'Image',
	);
	
	static $many_many = array(
		'FeaturedBylines' => 'Byline',
		'FeaturedMedia' => 'Media'
	);
 
	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Content.News', new TextField('NewsTitle1', 'News Item 1 Title'));
		$fields->addFieldToTab('Root.Content.News', new TextField('NewsURL1', 'News Item 1 URL'));
		$fields->addFieldToTab('Root.Content.News', new ImageField('NewsThumb1', 'News Item 1 Thumb', '', '', '', $folderName = 'HomeNews/'));

		$fields->addFieldToTab('Root.Content.News', new TextField('NewsTitle2', 'News Item 2 Title'));
		$fields->addFieldToTab('Root.Content.News', new TextField('NewsURL2', 'News Item 2 URL'));
		$fields->addFieldToTab('Root.Content.News', new ImageField('NewsThumb2', 'News Item 2 Thumb', '', '', '', $folderName = 'HomeNews/'));

		$fields->addFieldToTab('Root.Content.News', new TextField('NewsTitle3', 'News Item 3 Title'));
		$fields->addFieldToTab('Root.Content.News', new TextField('NewsURL3', 'News Item 3 URL'));
		$fields->addFieldToTab('Root.Content.News', new ImageField('NewsThumb3', 'News Item 3 Thumb', '', '', '', $folderName = 'HomeNews/'));

		$fields->addFieldToTab('Root.Content.News', new TextField('NewsTitle4', 'News Item 4 Title'));
		$fields->addFieldToTab('Root.Content.News', new TextField('NewsURL4', 'News Item 4 URL'));
		$fields->addFieldToTab('Root.Content.News', new ImageField('NewsThumb4', 'News Item 4 Thumb', '', '', '', $folderName = 'HomeNews/'));

		$byline_MMDOM = new ManyManyDataObjectManager(
			$this,
			'FeaturedBylines',
			'Byline',
			array(
				'Title' => 'Title'
			),
			'',
			'',
			'Date DESC'
		);

		$byline_MMDOM->setPermissions(array());

		$fields->addFieldToTab('Root.Content.FeaturedBylines', $byline_MMDOM);


		$media_MMDOM = new ManyManyDataObjectManager(
			$this,
			'FeaturedMedia',
			'Media',
			array(
				'Title' => 'Title'
			),
			'',
			'',
			'Title ASC'
		);

		$media_MMDOM->setPermissions(array());

		$fields->addFieldToTab('Root.Content.FeaturedMedia', $media_MMDOM );

		return $fields;
	}
}
 
class HomePage_Controller extends Page_Controller {

	function getFutureEvents(){
		// get 6 future events

		$ReturnedEvents = new DataObjectSet();

		//counter for number of events
		$numEvents = 0;
		
		$futureEvents = new DataObjectSet();

		$now = date('Y-m-d');

		$futureEvents = DataObject::get(
			$callerClass = "EventPage",
			$filter = "Date >= '$now'",
			$sort = "Date ASC",
			$join = "",
			$limit = "6"
		);

		if($futureEvents){
			$futureEvents->sort('Date');
		}
	
		$current_month = '';

		foreach($futureEvents as $futureEvent){
			if(substr($current_month, 5, -3) != substr($futureEvent->Date, 5, -3)){
			// this a new month
				$current_month = $futureEvent->Date;
				$ReturnedEvents->push(new ArrayData(array(
					'EventPage' => $futureEvent,
					'NewMonth' => $futureEvent
				)));
			} else {
			// not a new month
				$ReturnedEvents->push(new ArrayData(array(
					'EventPage' => $futureEvent,
					'NewMonth' => ''
				)));
			}
		}
	
		return $ReturnedEvents;
	
	}//getFutureEvents

	function getFeaturedBylines(){
		$FeaturedBylines = new DataObjectSet();
		$FeaturedBylines = $this->FeaturedBylines();
		$FeaturedBylines->sort('Date DESC');
		return $FeaturedBylines;
	}// getFeaturedBylines


	function getFeaturedMedia(){
		$FeaturedMedia = new DataObjectSet();
		$FeaturedMedia = $this->FeaturedMedia();
		$FeaturedMedia->sort('Created DESC');
		return $FeaturedMedia;
	}// getFeaturedMedia

}

?>
