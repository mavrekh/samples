<?php
 
class JobSubmitPage extends Page {
	static $db = array();

	static $has_one = array();
   	
	function getCMSFields() {
		$fields = parent::getCMSFields();
		return $fields;
	}
   
}
 
class JobSubmitPage_Controller extends Page_Controller {

	function init() {
	    parent::init();
         
		Validator::set_javascript_validation_handler('none');      

		Requirements::javascript("http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js");

		Requirements::css("themes/nyu-journalism/css/forms.css");

		Requirements::customScript('
			$(document).ready(function() {
				$("#Form_Form").validate({
					rules: {
						Title: "required",
						MediaOrganization: "required",
						JobLocationCity: "required",
						JobLocationState: "required",
						ListingType: "required",
						AcademicCreditRequired: "required",
						JobDescription: "required",
						CompanyDescription: "required",
						SizeofPaidStaff: "required",
						WillStudentWorkInNewsRoom: "required",
						JobSalaryRange: "required",
						ContributorPayRate: "required",
						ApplicationProcedure: "required",
						ContactName: "required",
						ContactTitle: "required",
						ContactEmail: {
							required: true,
							email: true
						},
						ContactPhone: "required",
						ContactFax: "required",
						ContactWebsite: {
							url: true
						},
						ContactAddress: "required",
						ContactCity: "required",
						ContactState: "required",
						ContactZip: "required"
					},
					messages: {
						ContactEmail: "A valid email address is required.",
						ContactWebsite: "A valid URL is required, ie <em>http://www.example.com</em>"
					}
				});

				$("#Form_Form_ListingType").change(function() {
					if($(this).val().indexOf("Internship") >= 0) {
						$("#TrainingElements").show();
					} else {
						$("#TrainingElements").hide();
					}
				});

			});
		');
	} //init

	function Form(){

		$fields = new FieldSet(
			new TextField('Title', 'Job Title'),
			new TextField('MediaOrganization', 'Media Organization'),	
			new TextField('JobLocationCity', 'Job Location City'),	

			new DropdownField('JobLocationState', 'Job Location State', array(
				'NY' => 'New York',
				'AL' => 'Alabama',
				'AK' => 'Alaska',
				'AZ' => 'Arizona',
				'AR' => 'Arkansas',
				'CA' => 'California',
				'CO' => 'Colorado',
				'CT' => 'Connecticut',
				'DE' => 'Delaware',
				'DC' => 'District of Columbia',
				'FL' => 'Florida',
				'GA' => 'Georgia',
				'HI' => 'Hawaii',
				'ID' => 'Idaho',
				'IL' => 'Illinois',
				'IN' => 'Indiana',
				'IA' => 'Iowa',
				'KS' => 'Kansas',
				'KY' => 'Kentucky',
				'LA' => 'Louisiana',
				'ME' => 'Maine',
				'MD' => 'Maryland',
				'MA' => 'Massachusetts',
				'MI' => 'Michigan',
				'MN' => 'Minnesota',
				'MS' => 'Mississippi',
				'MO' => 'Missouri',
				'MT' => 'Montana',
				'NE' => 'Nebraska',
				'NV' => 'Nevada',
				'NH' => 'New Hampshire',
				'NJ' => 'New Jersey',
				'NM' => 'New Mexico',
				'NC' => 'North Carolina',
				'ND' => 'North Dakota',
				'OH' => 'Ohio',
				'OK' => 'Oklahoma',
				'OR' => 'Oregon',
				'PA' => 'Pennsylvania',
				'RI' => 'Rhode Island',
				'SC' => 'South Carolina',
				'SD' => 'South Dakota',
				'TN' => 'Tennessee',
				'TX' => 'Texas',
				'UT' => 'Utah',
				'VT' => 'Vermont',
				'VA' => 'Virginia',
				'WA' => 'Washington',
				'WV' => 'West Virginia',
				'WI' => 'Wisconsin',
				'WY' => 'Wyoming',
				'OTHER' => 'Other'
			)),

			new DropdownField('ListingType', 'Listing Type', array(
				'Journalism Internship' => 'Journalism Internship',
				'Paid Journalism Internship' => 'Paid Journalism Internship',
				'Journalism Job' => 'Journalism Job',
				'Media-Related Job' => 'Media-Related Job',
				'Contributor' => 'Contributor',
			)),


/*
			new DropdownField('Medium', 'Medium', array(
				'Print' => 'Print',
				'Online' => 'Online',
				'Broadcast/Video' => 'Broadcast/Video'
			)),
*/

			new DropdownField('AcademicCreditRequired', 'Academic Credit Required', array(
				'Yes' => 'Yes',
				'No' => 'No',
			)),
			
			new DropdownField('PaidPosition', 'Is this a Paid Position?', array(
				'Yes' => 'Yes',
				'No' => 'No',
			)),
			
			new TextareaField('JobDescription', 'Job Description'),	
			
			new TextareaField('TrainingElements', '<span style="font-size:smaller;">Please tell us what training elements your internship will provide (speaker series, workshops, technical instruction, pitch/assignment feedback, etc.)</span>'),	
			new TextareaField('CompanyDescription', 'Company Description'),
			new TextField('SizeofPaidStaff', 'Size of Paid Editorial Staff'),
			new DropdownField('WillStudentWorkInNewsRoom', 'Will Student Work In News Room', array(
				'Yes' => 'Yes',
				'No' => 'No',
			)),
			new TextField('JobSalaryRange', 'Job Salary Range'),
			new TextField('ContributorPayRate', 'Contributor Pay Rate'),
			
			new TextareaField('ApplicationProcedure', 'Application Procedure'),	
			new TextField('ContactName', 'Contact Name <br /><em>(Not Published)</em>'),	
			new TextField('ContactTitle', 'Contact Title <br /><em>(Not Published)</em>'),	
			new TextField('ContactEmail', 'Contact Email <br /><em>(Not Published)</em>'),	
			new TextField('ContactPhone', 'Contact Phone <br /><em>(Not Published)</em>'),	
			new TextField('ContactFax', 'Contact Fax <br /><em>(Not Published)</em>'),	
			new TextField('ContactWebsite', 'Contact Website'),	
			new TextField('ContactAddress', 'Contact Address <br /><em>(Not Published)</em>'),	
			new TextField('ContactCity', 'Contact City <br /><em>(Not Published)</em>'),	
			new DropdownField('ContactState', 'Contact State <br /><em>(Not Published)</em>', array(
				'NY' => 'New York',
				'AL' => 'Alabama',
				'AK' => 'Alaska',
				'AZ' => 'Arizona',
				'AR' => 'Arkansas',
				'CA' => 'California',
				'CO' => 'Colorado',
				'CT' => 'Connecticut',
				'DE' => 'Delaware',
				'DC' => 'District of Columbia',
				'FL' => 'Florida',
				'GA' => 'Georgia',
				'HI' => 'Hawaii',
				'ID' => 'Idaho',
				'IL' => 'Illinois',
				'IN' => 'Indiana',
				'IA' => 'Iowa',
				'KS' => 'Kansas',
				'KY' => 'Kentucky',
				'LA' => 'Louisiana',
				'ME' => 'Maine',
				'MD' => 'Maryland',
				'MA' => 'Massachusetts',
				'MI' => 'Michigan',
				'MN' => 'Minnesota',
				'MS' => 'Mississippi',
				'MO' => 'Missouri',
				'MT' => 'Montana',
				'NE' => 'Nebraska',
				'NV' => 'Nevada',
				'NH' => 'New Hampshire',
				'NJ' => 'New Jersey',
				'NM' => 'New Mexico',
				'NC' => 'North Carolina',
				'ND' => 'North Dakota',
				'OH' => 'Ohio',
				'OK' => 'Oklahoma',
				'OR' => 'Oregon',
				'PA' => 'Pennsylvania',
				'RI' => 'Rhode Island',
				'SC' => 'South Carolina',
				'SD' => 'South Dakota',
				'TN' => 'Tennessee',
				'TX' => 'Texas',
				'UT' => 'Utah',
				'VT' => 'Vermont',
				'VA' => 'Virginia',
				'WA' => 'Washington',
				'WV' => 'West Virginia',
				'WI' => 'Wisconsin',
				'WY' => 'Wyoming',
				'OTHER' => 'Other'
			)),
			
			new TextField('ContactZip', 'Contact Zip <br /><em>(Not Published)</em>'),
			
			new TextField('Challenge', '<em>Complete this problem to prove you are not spam:</em> <br /><strong>1 + 1 =?</strong>')
			
		);
		
		$actions = new FieldSet(
			new FormAction ('doSubmitJob', 'Submit')
		);
	
		$validator = new RequiredFields(
			'Title',
			'MediaOrganization',
			'JobLocationCity',
			'JobLocationState',
			'ListingType',
			'AcademicCreditRequired',
			'JobDescription',
			'CompanyDescription',
			'SizeofPaidStaff',
			'WillStudentWorkInNewsRoom',
			'JobSalaryRange',
			'ContributorPayRate',
			'ApplicationProcedure',
			'ContactName',
			'ContactTitle',
			'ContactEmail',
			'ContactPhone',
			'ContactFax',
			'ContactWebsite',
			'ContactAddress',
			'ContactCity',
			'ContactState',
			'ContactZip',
			'Challenge'
		);

		$form = new Form(
			$this,
			'Form',
			$fields,
			$actions,
			$validator
		);
		$form->disableSecurityToken();	// for now, until I figure out a better way to refresh this form when session expires
		
		return $form;
	} // Form

	function doSubmitJob($data, $form){	
		$job = new Job();
		$form->saveInto($job);
		
		if($job->Challenge != '2'){
			echo '<p>You do not know what 1 + 1 is?  You must be a spammer, go away!</p>';
			echo '<p><em>If you are <strong>not</strong> a spammer, and need help with this, please email Sylvan Solloway: <a href="mailto:sylvan.solloway@nyu.edu">sylvan.solloway@nyu.edu</a>.</p>';
			return;
		}
		
		$job->Approved = 'Pending';
		$job->SendToList = 'No';
		$job->Date = date("Y-m-d");
		$job->write();
		
		$form->sessionMessage(
			'Form successfully submitted',
			'good'
		);
		
		Director::redirect("/thank-you-for-your-submission");
		
		// send message with job info to career services
		$message = '<html><body style="background: #EEE; color: #000;"><div id="message" style="border: 1px solid black; padding: 10px; margin: 10px; background: #FFF;">';
		$message .= '<p><img src="http://journalism.nyu.edu/etc/common/logo.png" alt="NYU Journalism Logo" /></p>';
		$message .= '<h2>Job Alert from NYU Journalism</h2>';
		$message .= '<p><strong>Job Title</strong> '.$job->Title.'</p>';
		$message .= '<p><strong>Media Organization</strong> '.$job->MediaOrganization.'</p>';
		$message .= '<p><strong>Organization Description</strong><br /> '.nl2br($job->CompanyDescription).'</p>';

		$message .= '<p><strong>Where</strong> '.$job->JobLocationCity.', '.$job->JobLocationState.'</p>';
		$message .= '<p><strong>Listing Type</strong> '.$job->ListingType.'</p>';
		$message .= '<p><strong>Is this a paid position?</strong> '.$job->PaidPosition.'</p>';
		//$message .= '<p><strong>Medium</strong> '.$job->Medium.'</p>';
		$message .= '<p><strong>Is academic credit required?</strong> '.$job->AcademicCreditRequired.'</p>';
		$message .= '<p><strong>Job Description</strong><br /> '.nl2br($job->JobDescription).'</p>';
		$message .= '<p><strong>Training Elements (if internship)</strong><br /> '.nl2br($job->TrainingElements).'</p>';
		$message .= '<p><strong>How to Apply</strong><br /> '.nl2br($job->ApplicationProcedure).'</p>';

		//contact info, this is not to go to list, only internal
		$message .= "<p><strong>Contact Info</strong></p>";
		$message .= '<p><strong>Contact Name</strong> '.$job->ContactName.'</p>';
		$message .= '<p><strong>Contact Title</strong> '.$job->ContactTitle.'</p>';
		$message .= '<p><strong>Contact Email</strong> '.$job->ContactEmail.'</p>';
		$message .= '<p><strong>Contact Phone</strong> '.$job->ContactPhone.'</p>';
		$message .= '<p><strong>Contact Fax</strong> '.$job->ContactFax.'</p>';
		$message .= '<p><strong>Contact Website</strong> '.$job->ContactWebsite.'</p>';
		$message .= '<p><strong>Contact Address</strong> '.$job->ContactAddress.'</p>';
		$message .= '<p><strong>Contact City</strong> '.$job->ContactCity.'</p>';
		$message .= '<p><strong>Contact State</strong> '.$job->ContactState.'</p>';
		$message .= '<p><strong>Contact Zip</strong> '.$job->ContactZip.'</p>';

		if ($job->ContactWebsite){
			$message .= '<p><strong>Website: </strong><a href="'.$job->ContactWebsite.'">'.$job->ContactWebsite.'</p>';
		}
				
		$message .= '<div></body></html>';

		$headers = "From: NYU Journalism <journalism.webmaster@nyu.edu>\r\nContent-type: text/html; charset=iso-8859-1\r\n";
				
		mail('sylvan.solloway@nyu.edu', 'NYU Journalism Job Alert: '.$job->Title, $message, $headers);
		
		return;
		
	}// doSubmitJob

}
?>
