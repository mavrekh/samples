	<div id="hd">
		<div class="inner">
			<h1>Career Services</h1>
			<h2>From the minute you begin your journalism studies, you should be preparing for internships and a challenging career after graduation. We’re here to help.</h2>
			<ul id="hd-menu">
				<li><a href="/career-services/">Overview</a></li>
          		<% control Siblings %>
          			<% if LinkOrCurrent = current %>
          				<li class="active"><a href="$Link">$MenuTitle.XML</a></li>
          			<% else %>
                  		<li><a href="$Link" title="Go to the $Title.XML page">$MenuTitle.XML</a></li>
                	<% end_if %>
              	<% end_control %>
			</ul>
			<div class="clr"><!-- clear --></div>
		</div>	
	</div>

	<div id="holder">
		<h2 class="page-title">$Title</h2>
		<!-- Cover Page for PopUps -->
		<div class="cover"></div>


		<% if JInternships %>
		<div class="unit-section-holder">
			<div class="unit-section-header clickable">&raquo; Internships</div>
			<div class="unit-section-course-list">
				<% control getJInternships %>
					<div class="course-row item-header clickable">
						$Title &mdash; $MediaOrganization
					</div>
					<div class="job-detail item-detail">
						<h1>$Title at <% if ContactWebsite %><a href="$ContactWebsite" target="_blank"><% end_if %>$MediaOrganization<% if ContactWebsite %></a><% end_if %></h1>
						<p><em>$Date &raquo; $ListingType &raquo; $Medium</em></p>

						<h2>Company Description</h2>
						<p>$CompanyDescription</p>

						<h2>Job Description</h2>
						<p>$JobDescription</p>

						<h2>To Apply</h2>
						<p>$ApplicationProcedure</p>

						<p><span class="clickable jobs-close item-close">[x] close.</span></p>
					</div>
				<% end_control %>
			</div>
		</div>
		<% end_if %>



		<% if PaidJInternships %>
		<div class="unit-section-holder">
			<div class="unit-section-header clickable">&raquo; Paid Internships</div>
			<div class="unit-section-course-list">
				<% control getPaidJInternships %>
					<div class="course-row item-header clickable">
						$Title &mdash; $MediaOrganization
					</div>
					<div class="job-detail item-detail">
						<h1>$Title at <% if ContactWebsite %><a href="$ContactWebsite" target="_blank"><% end_if %>$MediaOrganization<% if ContactWebsite %></a><% end_if %></h1>
						<p><em>$Date &raquo; $ListingType &raquo; $Medium</em></p>

						<h2>Company Description</h2>
						<p>$CompanyDescription</p>

						<h2>Job Description</h2>
						<p>$JobDescription</p>

						<h2>To Apply</h2>
						<p>$ApplicationProcedure</p>

						<p><span class="clickable jobs-close item-close">[x] close.</span></p>
					</div>
				<% end_control %>
			</div>
		</div>
		<% end_if %>



		<% if JJobs %>
		<div class="unit-section-holder">
			<div class="unit-section-header clickable">&raquo; Journalism Jobs</div>
			<div class="unit-section-course-list">
				<% control getJJobs %>
					<div class="course-row item-header clickable">
						$Title &mdash; $MediaOrganization
					</div>
					<div class="job-detail item-detail">
						<h1>$Title at <% if ContactWebsite %><a href="$ContactWebsite" target="_blank"><% end_if %>$MediaOrganization<% if ContactWebsite %></a><% end_if %></h1>
						<p><em>$Date &raquo; $ListingType &raquo; $Medium</em></p>

						<h2>Company Description</h2>
						<p>$CompanyDescription</p>

						<h2>Job Description</h2>
						<p>$JobDescription</p>

						<h2>To Apply</h2>
						<p>$ApplicationProcedure</p>

						<p><span class="clickable jobs-close item-close">[x] close.</span></p>
					</div>
				<% end_control %>
			</div>
		</div>
		<% end_if %>



		<% if MediaRelatedJobs %>
		<div class="unit-section-holder">
			<div class="unit-section-header clickable">&raquo; Media-Related Jobs</div>
			<div class="unit-section-course-list">
				<% control getMediaRelatedJobs %>
					<div class="course-row item-header clickable">
						$Title &mdash; $MediaOrganization
					</div>
					<div class="job-detail item-detail">
						<h1>$Title at <% if ContactWebsite %><a href="$ContactWebsite" target="_blank"><% end_if %>$MediaOrganization<% if ContactWebsite %></a><% end_if %></h1>
						<p><em>$Date &raquo; $ListingType &raquo; $Medium</em></p>

						<h2>Company Description</h2>
						<p>$CompanyDescription</p>

						<h2>Job Description</h2>
						<p>$JobDescription</p>

						<h2>To Apply</h2>
						<p>$ApplicationProcedure</p>

						<p><span class="clickable jobs-close item-close">[x] close.</span></p>
					</div>
				<% end_control %>
			</div>
		</div>
		<% end_if %>




		<% if ContributorJobs %>
		<div class="unit-section-holder">
			<div class="unit-section-header clickable">&raquo; Contributor Opportunities</div>
			<div class="unit-section-course-list">
				<% control getContributorJobs %>
					<div class="course-row item-header clickable">
					<!--<div class="job-header clickable item-header">-->
						$Title &mdash; $MediaOrganization
					</div>
					<div class="job-detail item-detail">
						<h1>$Title at <% if ContactWebsite %><a href="$ContactWebsite" target="_blank"><% end_if %>$MediaOrganization<% if ContactWebsite %></a><% end_if %></h1>
						<p><em>$Date &raquo; $ListingType &raquo; $Medium</em></p>

						<h2>Company Description</h2>
						<p>$CompanyDescription</p>

						<h2>Job Description</h2>
						<p>$JobDescription</p>

						<h2>To Apply</h2>
						<p>$ApplicationProcedure</p>

						<p><span class="clickable jobs-close item-close">[x] close.</span></p>
					</div>
				<% end_control %>
			</div>
		</div>
		<% end_if %>




	</div>
