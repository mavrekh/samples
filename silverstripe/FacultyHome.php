<?php
/**
 * Defines the FacultyHome page type
 */
class FacultyHome extends Page {
   static $db = array(
   );
   static $has_one = array(
   );
   
   static $allowed_children = array('FacultyPage');
}
 
class FacultyHome_Controller extends Page_Controller {

	function getAllFaculty() {
		return $this->getFacultyPages();
	}

	function getProfessors() {
		return $this->getFacultyPages(" FacultyType = 'Professor' OR 
										FacultyType = 'Professor Emeritus' OR
										FacultyType = 'Associate Professor' OR 
										FacultyType = 'Assistant Professor' OR 
										FacultyType = 'Clinical Professor' OR
										FacultyType = 'Clinical Associate Professor' OR
										FacultyType = 'Clinical Assistant Professor' OR
										FacultyType = 'Visiting Assistant Professor' OR
										FacultyType = 'Visiting Associate Professor'
									");
	}

	function getAdjuncts() {
		return $this->getFacultyPages("FacultyType = 'Adjunct Faculty'");
	}

	function getAbroadFaculty() {
		return $this->getFacultyPages("FacultyType = 'Abroad'");
	}

	function getVisitingScholars() {
		return $this->getFacultyPages("FacultyType = 'Visiting Scholar'");
	}

	function getWritersInResidence() {
		return $this->getFacultyPages("FacultyType = 'Distinguished Writer in Residence'");
	}


	function getFacultyPages($filter){
		// this funciton gets faculty pages based on filter criteria
		
		return DataObject::get('FacultyPage', $filter);
	}

}

?>
