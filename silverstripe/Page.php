<?php
class Page extends SiteTree {

	public static $db = array();

	public static $has_one = array();

	/**
	* Return a list of all the pages to cache
	*/
	function allPagesToCache() {
		error_log('building the cache!');

		// Get each page type to define its sub-urls
		$urls = array();
		
		// memory intensive depending on number of pages

		$pages = DataObject::get("SiteTree");
		
		// full build - omits 'live' pages
//		$ignored = array('JobSubmitPage', 'JobsListingPage', 'UnCachedPage');
		$ignored = array('JobSubmitPage', 'UnCachedPage');
		
		// partial build in case we need the major pages done fast
//		$ignored = array('JobSubmitPage', 'JobsListingPage', 'UnCachedPage', 'CoursePage', 'Page', 'NewsPage', 'EventPage');
 
		foreach($pages as $page) {
			if(!in_array($page->ClassName, $ignored)){
				$urls = array_merge($urls, (array)$page->subPagesToCache());
			}
		}

		// add any custom URLs which are not SiteTree instances
//		$urls[] = "sitemap.xml";

		return $urls;
	}

	/**
	* Get a list of URLs to cache related to this page
	*/
	function subPagesToCache() {
		$urls = array();

		// add current page
		$urls[] = $this->Link();

		return $urls;
	}

	function pagesAffectedByChanges() {
		$urls = $this->subPagesToCache();
		if($p = $this->Parent) $urls = array_merge((array)$urls, (array)$p->subPagesToCache());
		return $urls;
	}

}

class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	public static $allowed_actions = array ();

	public function init() {
		parent::init();
	}

	public function nextPager() {
		$where = "ParentID = ($this->ParentID + 0) AND Sort > ($this->Sort + 0 )";
		$pages = DataObject::get("SiteTree", $where, "Sort", "", 1);
		if($pages) {
			foreach($pages as $page) { return $page;}
		}
	} // nextPager
	
	public function previousPager() {
		$where = "ParentID = ($this->ParentID + 0) AND Sort < ($this->Sort + 0)";
   		$pages = DataObject::get("SiteTree", $where, "Sort DESC", "", 1);
		if($pages) {
			foreach($pages as $page) { return $page; }
		}
	} // previousPager

	/*
	*
	* THESE ARE OUR CUSTOM FUNCTIONS
	* WE WANT AVAILABLE FOR ALL PAGES
	*
	*/

	function getFeaturedMedia(){
		//relies on the existence of a has_many featured media
		return $this->FeaturedMedia();
	} //getFeaturedMedia

	function getSiblings() {
		// return all pages with the same parent ID who are also enabled for show in menus
		//
		return DataObject::get("Page", "ParentID = ".$this->ParentID." AND ShowInMenus = 1");
	}
	
	function getHiddenSiblings() {
		// return all pages with the same parent ID, regardless of menu enabled
		//
		return DataObject::get("Page", "ParentID = ".$this->ParentID);
	}
	
}
?>