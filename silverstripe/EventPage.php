<?php
/**
 * Defines the EventPage page type
 */
class EventPage extends Page {
	static $db = array(
		'Date' => 'Date',
		'EndDate' => 'Date',
		'Time' => 'Varchar(255)',
		'Location' => 'Varchar(255)',
	);
	
	static $has_one = array(
		'Image' => 'Image',
	);

	static $many_many = array (
		'Units' => 'Unit',
	);
/*
	function getCMSValidator(){
		return new EventPage_Validator();
	}
*/
	function getTime() {
		## Set time stamps for the calendar etc links. MA March 2013 ##

		$fullTime = $this->getField("Time");
		$bdate = $this->getField("Date");
		$edate = $this->getField("EndDate");

		// split time stamp into pieces for the calendar links
		list($begin, $end) = explode('-', $fullTime);

		$end = str_replace('.','', trim($end));	// avoid confusion between p.m. and pm, also trim whitespace
		$begin = str_replace('.','', trim($begin));

		// if only the end time has an am/pm label, assume that the beginning is also in the am/pm
		if ($begin && $end && (stripos($begin,'m') === false) && (stripos($end,'m') !== false) ) {
			$begin .= substr($end, -2);
		}

		$fullDateBeginTime = "$bdate $begin America/New_York";	// all events are entered in NYC time
		$fullDateBeginTimeUnix = strtotime($fullDateBeginTime);

		if ($end) {
			$fullDateEndTime = "$bdate $end America/New_York";
			$fullDateEndTimeUnix = strtotime($fullDateEndTime);
		} else {
			// if end time is not specified, assume it's one hour after the beginning
			$fullDateEndTimeUnix = $fullDateBeginTimeUnix + 3600;
		}

		// Only display links if we have valid timestamps. Also $edate implies "various dates", in which case we don't display the links
		if ($fullDateBeginTimeUnix && !$edate) {
			$this->BeginTime = gmstrftime('%Y%m%dT%H%M%SZ',$fullDateBeginTimeUnix);	// gcal and others want their time stamps in GMT
			$this->BeginTimeLabel = $begin;
			$this->EndTime = gmstrftime('%Y%m%dT%H%M%SZ',$fullDateEndTimeUnix);
		} else {
			$this->BeginTime = 0;
		}

		return $fullTime;
	}


/*
	function getLink() {
		$link = $this->getField("Link");
		print $link;

		return $link . 'AAA';
	}
*/



	function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.Content.Main', new DatePickerField('Date','Date'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new DatePickerField('EndDate','End Date (optional)'), 'Content');

		$fields->addFieldToTab('Root.Content.Main', new TextField('Time', 'Time'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new TextField('Location', 'Location'), 'Content');
		$fields->addFieldToTab('Root.Content.Main', new ImageField('Image', 'Image', '', '', '', $folderName = 'Events/'), 'Content');

		$unitsMMDOM = new ManyManyDataObjectManager(
			$this,
			'Units',
			'Unit',
			array('Name' => 'Unit Name')
		);

		// we don't want people adding new units!
		$unitsMMDOM->setPermissions(array());

		$fields->addFieldToTab('Root.Content.Units', $unitsMMDOM);
		
		return $fields;
	}

}

class EventPage_Controller extends Page_Controller {}



class EventPage_Validator extends Validator{
	function javascript(){
		return false;
	}

	function php($data){
		$bRet = true;
		
		// core requirements
		
		if (!$data['Title'] || strlen($data['Title']) > 100){
			$this->validationError(
				'Form_EditForm_Title',
				'Title is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['MenuTitle'] || strlen($data['MenuTitle']) > 100){
			$this->validationError(
				'Form_EditForm_MenuTitle',
				'MenuTitle is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Content']){
			$this->validationError(
				'Form_EditForm_Content',
				'Content is Missing',
				"required"
			);

			$bRet = false;
		}

		// additional requirements
		/*
		if (!RelationshipValidator::has_file('EventPage', $data['ID'], 'ImageID')){
			$this->validationError(
				'Form_EditForm_Image',
				'We need an image',
				"required"
			);

			$bRet = false;
		}
		*/
		
		if (!$data['Location'] || strlen($data['Location']) > 255){
			$this->validationError(
				'Form_EditForm_Location',
				'Location is Either Missing or Longer than 255 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Time'] || strlen($data['Time']) > 255){
			$this->validationError(
				'Form_EditForm_Time',
				'Time is Either Missing or Longer than 255 Characters',
				"required"
			);

			$bRet = false;
		}

		if (!$data['Date']){
			$this->validationError(
				'Form_EditForm_Date',
				'Date is Required',
				"required"
			);

			$bRet = false;
		}
		
		return $bRet;
	}
}
?>
