<?php

class Person extends DataObject {
	static $db = array(
		'Type' => 'Varchar(255)',
		'FirstName' => 'Varchar(255)',
		'LastName' => 'Varchar(255)',
		'GradProgramYear' => 'Varchar(255)'
	);
	
	static $has_one = array();

	static $many_many = array (
		'Units' => 'Unit',
	);
	
	static $searchable_fields = array(
		'Type',
		'FirstName',
		'LastName'
	);

	static $summary_fields = array(
		'LastName',
		'FirstName',
		'Type',
	);
	
	static $belongs_many = array(
		'Bylines' => 'Byline'
	);
	
	static $belongs_many_many = array (
		'Courses' => 'CoursePage'
	);
	
	static $default_sort = 'LastName ASC';

	function getCMSValidator(){
		return new Person_Validator();
	}	
	
	function getCMSFields() {
		$fields = parent::getCMSFields();

 		$fields->addFieldToTab('Root.Main', new DropdownField('Type', 'Type', array(
				'Alumni' => 'Alumni', 
				'Faculty' => 'Faculty', 
				'Staff' => 'Staff',
				'Student' => 'Student',
			))
		);

		$unitsMMCTF = new ManyManyDataObjectManager(
			$this,
			'Units',
			'Unit',
			array('Name' => 'Unit Name'),
			'',
			'',
			'Name ASC'
		);

		// adding and removing units is a big deal, so we lock it out by setting permissions to allow nothing
		// however, IF it needs to be done comment out this line of code which will then allow it
//		$unitsMMCTF->setPermissions(array());

		$fields->addFieldToTab('Root.Units', $unitsMMCTF);

		return $fields;
	}
	
}

class Person_Validator extends Validator{
	function javascript(){
		return false;
	}

	function php($data){
		$bRet = true;

		if (!$data['FirstName'] || strlen($data['FirstName']) > 100){
			$this->validationError(
				'FirstName',
				'FirstName is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}


		if (!$data['LastName'] || strlen($data['LastName']) > 100){
			$this->validationError(
				'LastName',
				'LastName is Either Missing or Longer than 100 Characters',
				"required"
			);

			$bRet = false;
		}

		return $bRet;
	}
}

?>
